﻿using BoYu66.Services.Bonus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusWebAPI.Models
{

    public class SetBonusModel : BonusRuleModel
    {
        public string EventClassType { get; set; }
        public int RequireInSuccessionTimes { get; set; }
        public Rule[] rules { get; set; }
    }

    public class Rule
    {
        public string RuleType { get; set; }
        public string VerifyType { get; set; }
        public int MinDepositAmount { get; set; }
    }

}