﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusWebAPI.Models
{
    public class PlayerActionModel
    {
        public string action { get; set; }
        public decimal amount { get; set; }
        public string platformCode { get; set; }
        public string TransactionID { get; set; }
    }


}