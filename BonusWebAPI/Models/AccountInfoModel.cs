﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusWebAPI.Models
{
    public class AccountInfoModel
    {
        public decimal ActualBalance { get; set; }
        public decimal BonusWallet { get; set; }
        public decimal WithdrawLimit { get; set; }
        public decimal TurnoverAmount { get; set; }
    }

}