﻿using BoYu66.Services.Billing;
using BoYu66.Services.Bonus;
using BoYu66.Services.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusWebAPI
{
    public static class BonusHelper
    {
        private static Dictionary<string, EnginePack> _session = new Dictionary<string, EnginePack>();


        public static EnginePack GetCurrentEngines()
        {
            return GetCurrentEngines(HttpContext.Current.Request.Cookies["userid"].Value);
        }

        private static EnginePack GetCurrentEngines(string userid)
        {
            if (_session.ContainsKey(userid) == false)
            {
                var billingEngine = new BillingEngine();
                var memberEngine = new MemberEngine();
                _session[userid] = new EnginePack()
                {
                    billing = billingEngine,
                    member = memberEngine,
                    bonus = new BonusEngine(billingEngine, memberEngine)
                };
                _session[userid].bonus.RuleCompleted += (rule) =>
                {
                    rule.Complete(_session[userid].billing, _session[userid].bonus);
                };
            }

            NLog.LogManager.GetCurrentClassLogger().Info("Init");

            return _session[userid];
        }

        public static void ResetCurrentEngines(string userid)
        {
            if (_session.ContainsKey(userid)) _session.Remove(userid);
        }
    }

    public class EnginePack
    {
        public BillingEngine billing { get; set; }
        public BonusEngine bonus { get; set; }
        public MemberEngine member { get; set; }
    }
    
}