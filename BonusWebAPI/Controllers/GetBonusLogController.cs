﻿using BoYu66.Services.Bonus;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace BonusWebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GetBonusLogController : ApiController
    {
        public JArray Post()
        {
            NLog.LogManager.GetCurrentClassLogger().Info("GetBonusLog");
            try
            {
                var bonusLogList = BonusHelper.GetCurrentEngines().bonus.GetLogList();

                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(bonusLogList);

                var result = JArray.Parse(json);
                NLog.LogManager.GetCurrentClassLogger().Info(result);

                return result;

            }
            catch (Exception e)
            {
                //Init.InitAction() ;
                NLog.LogManager.GetCurrentClassLogger().Info(e.Message);
                return JArray.Parse("{'msg':'Please retry again'}");
            }

        }
    }
}
