﻿using BoYu66.Services.Bonus;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BonusWebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GetBonusStatusListController : ApiController
    {
        public JObject Post()
        {
            NLog.LogManager.GetCurrentClassLogger().Info("GetBonusStatusList");

            try
            {
                //有點作弊 因為RuleBase的 CompositeRulesEventBase  Get
                var bonusStatusList = BonusHelper.GetCurrentEngines().bonus.GetBonusStatusList();
                //BonusEngine.Current.GetBonusStatusList();
                var result = JObject.Parse(JsonConvert.SerializeObject(bonusStatusList));
                NLog.LogManager.GetCurrentClassLogger().Info(result);

                return result;


            }
            catch (Exception e)
            {
                //Init.InitAction();
                NLog.LogManager.GetCurrentClassLogger().Info(e.Message);
                return JObject.Parse("{'msg':'Please retry again'}");
            }
        }
    }
}
