﻿using BonusWebAPI.Models;
using BoYu66.Services.Billing;
using BoYu66.Services.Bonus;
using BoYu66.Services.Membership;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BonusWebAPI.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PlayerActionController : ApiController
    {
        public bool Post( [FromBody] PlayerActionModel playerActionModel)
        {
            NLog.LogManager.GetCurrentClassLogger().Info("PlayerAction");
            NLog.LogManager.GetCurrentClassLogger().Info(JObject.Parse(JsonConvert.SerializeObject(playerActionModel)));

            //JObject jsonObj = GetRequestResult();
            //var playerActionModel = jsonObj.ToObject<PlayerActionModel>();
            var accountInfo = new AccountInfoModel();
            accountInfo.ActualBalance = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.ActualBalance;
            accountInfo.BonusWallet = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.BonusWallet;
            accountInfo.TurnoverAmount = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.TurnoverAmount;
            accountInfo.WithdrawLimit = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.WithdrawLimit;
            BonusHelper.GetCurrentEngines().bonus.AddLogList($"BEFORE 主帳戶餘額: {accountInfo.ActualBalance}, 紅利錢包: {accountInfo.BonusWallet} , 提款限額: {accountInfo.WithdrawLimit} , 流水: {accountInfo.TurnoverAmount}");

            try
            {
                switch (playerActionModel.action)
                {
                    case "settle":
                        BonusHelper.GetCurrentEngines().billing.Settle(playerActionModel.TransactionID, playerActionModel.amount, Math.Abs(playerActionModel.amount), true, true, true, playerActionModel.platformCode, BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    case "deposit":
                        BonusHelper.GetCurrentEngines().billing.Deposit(playerActionModel.amount, BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    case "login":
                        BonusHelper.GetCurrentEngines().member.Login(BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    case "id-verify":
                        BonusHelper.GetCurrentEngines().member.VerifyAccount("id-verify", BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    case "phone-verify":
                        BonusHelper.GetCurrentEngines().member.VerifyAccount("phone-verify", BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    case "email-verify":
                        BonusHelper.GetCurrentEngines().member.VerifyAccount("email-verify", BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    case "register":
                        BonusHelper.GetCurrentEngines().member.RegisterAccount(BonusHelper.GetCurrentEngines().billing, BonusHelper.GetCurrentEngines().bonus);
                        break;
                    default:
                        NLog.LogManager.GetCurrentClassLogger().Info("false");
                        return false;
                }
            }
            catch (Exception e)
            {
                //Init.InitAction();
                NLog.LogManager.GetCurrentClassLogger().Info(e.Message);
                NLog.LogManager.GetCurrentClassLogger().Info("false");
                return false;
            }

            accountInfo.ActualBalance = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.ActualBalance;
            accountInfo.BonusWallet = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.BonusWallet;
            accountInfo.TurnoverAmount = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.TurnoverAmount;
            accountInfo.WithdrawLimit = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.WithdrawLimit;
            BonusHelper.GetCurrentEngines().bonus.AddLogList($"AFTER 主帳戶餘額: {accountInfo.ActualBalance}, 紅利錢包: {accountInfo.BonusWallet}, , 提款限額: {accountInfo.WithdrawLimit} , 流水: {accountInfo.TurnoverAmount}");

            NLog.LogManager.GetCurrentClassLogger().Info("true");
            return true;
        }

        private JObject GetRequestResult()
        {
            var jsonStr = Request.Content.ReadAsStringAsync().Result;
            var jsonObj = JObject.Parse(jsonStr);
            return jsonObj;
        }
    }
}
