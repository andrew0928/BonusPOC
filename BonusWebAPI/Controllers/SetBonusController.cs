﻿using BonusWebAPI.Models;
using BoYu66.Services.Billing;
using BoYu66.Services.Bonus;
using BoYu66.Services.Common;
using BoYu66.Services.Membership;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BonusWebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SetBonusController : ApiController
    {
        //log
        public bool Post([FromBody]SetBonusModel setBonusModel)
        {
            NLog.LogManager.GetCurrentClassLogger().Info(JObject.Parse(JsonConvert.SerializeObject(setBonusModel)));
            try
            {
                //var clientIP = GetIPAddress();
                //JObject jsonObj = GetRequestResult();
                //var setBonusModel = jsonObj.ToObject<SetBonusModel>();
                //var bonusObj = jsonObj.ToObject<BonusRuleModel>();
                

                var eventClassType = GetType(setBonusModel.EventClassType);

                if (setBonusModel.TurnoverLimitRate == 0) setBonusModel.Priority = 1;
                var eventClassInstance = Activator.CreateInstance(eventClassType, setBonusModel);

                foreach (var r in setBonusModel.rules)
                {
                    var ruleType = GetType(r.RuleType);
                    RuleBase ruleInstance = (RuleBase)Activator.CreateInstance(ruleType);

                    if (ruleInstance is UserVerifyRule)
                    {
                        ((UserVerifyRule)ruleInstance).VerifyType = r.VerifyType;
                    }
                    else if (ruleInstance is DepositRule)
                    {
                        ((DepositRule)ruleInstance).MinDepositAmount = r.MinDepositAmount;
                    }

                    //rulesList.Add(ruleInstance);
                    if (eventClassInstance is TurnoverBonusEvent)
                    {
                        //BonusEngine.Current.RegisterEvent((TurnoverBonusEvent)eventClassInstance);
                    }
                    else
                    {
                        ((CompositeRulesEventBase)eventClassInstance).MatchRules.Add(ruleInstance);
                        //BonusEngine.Current.RegisterEvent((CompositeRulesEventBase)eventClassInstance);
                    }

                }
                //BonusEngine.Current.RegisterEvent((EventBase)eventClassInstance);
                BonusHelper.GetCurrentEngines().bonus.RegisterEvent((EventBase)eventClassInstance);

                NLog.LogManager.GetCurrentClassLogger().Info("true");

                return true;
            }
            catch (Exception e)
            {
                //Init.InitAction();
                NLog.LogManager.GetCurrentClassLogger().Info(e.Message);

            }
            NLog.LogManager.GetCurrentClassLogger().Info("false");

            return false;
        }

        private JObject GetRequestResult()
        {
            var jsonStr = Request.Content.ReadAsStringAsync().Result;
            var jsonObj = JObject.Parse(jsonStr);
            return jsonObj;
        }

        private string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string sIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(sIPAddress))
            {
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                string[] ipArray = sIPAddress.Split(new Char[] { ',' });
                return ipArray[0];
            }
        }

        private Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

    }
}
