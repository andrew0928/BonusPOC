﻿using BonusWebAPI.Models;
using BoYu66.Services.Billing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace BonusWebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GetAccountInfoController : ApiController
    {
        public JObject Post()
        {
            NLog.LogManager.GetCurrentClassLogger().Info("GetAccountInfo");

            try
            {
                var accountInfo = new AccountInfoModel();
                accountInfo.ActualBalance = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.ActualBalance;
                accountInfo.BonusWallet = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.BonusWallet;
                accountInfo.TurnoverAmount = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.TurnoverAmount;
                accountInfo.WithdrawLimit = BonusHelper.GetCurrentEngines().billing.CurrentAccountInfo.WithdrawLimit;

                var result = JObject.Parse(JsonConvert.SerializeObject(accountInfo));
                NLog.LogManager.GetCurrentClassLogger().Info(result);

                return result;
            }
            catch (Exception e)
            {
                //Init.InitAction();
                NLog.LogManager.GetCurrentClassLogger().Info(e.Message);
                return JObject.Parse("{'msg':'Please retry again'}");
            }
        }
    }
}
