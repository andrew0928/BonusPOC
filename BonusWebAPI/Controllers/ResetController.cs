﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BonusWebAPI.Controllers
{
    public class ResetController : ApiController
    {
        public bool Post()
        {
            BonusHelper.ResetCurrentEngines(HttpContext.Current.Request.Cookies["userid"].Value);
            return true;
        }
    }
}
