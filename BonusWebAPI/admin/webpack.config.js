var webpack = require('webpack');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin')
var path = require('path');

var APP_DIR = path.resolve(__dirname, 'app');
var BUILD_DIR = path.resolve(__dirname, 'dist');

var config = {
  entry: { 
    index: APP_DIR + '/app.js'
  },
  output: {
    path: BUILD_DIR,//BUILD_DIR,
    filename: "[name].js"
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel-loader'
      },
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!less-loader"
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
    ]
  },
  devServer: {
    inline:true,
    port: 8090
  },  
  plugins: [
    /*new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
     'process.env': {
       'NODE_ENV': JSON.stringify('production')
     }
    }),
    new UglifyJSPlugin()*/
  ]
};

module.exports = config;