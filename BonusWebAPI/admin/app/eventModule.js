import React from 'react';
import {render} from 'react-dom';

import EventIntro  from './eventIntro';
import EventSettings from './eventSettings';

import './eventModule.less';

export default class EventModule extends React.Component {
  render () {
  	const { type, onAddEventSettings } = this.props;

  	if (!type) return null;

    return <div className="eventModule">
    	{false && <EventIntro type={type} />}
    	<EventSettings type={type} onAddEventSettings={settings => onAddEventSettings(settings)} />
    </div>;
  }
}