import React from 'react';
import {render} from 'react-dom';
import SlidingPane from 'react-sliding-pane';

import 'react-sliding-pane/dist/react-sliding-pane.css';

import EventsBrowser from './eventsBrowser';
import EventAction from './eventAction';
import AccountBalance from './accountBalance';

class App extends React.Component {
  constructor(props) {
  	super(props);

  	this.state = {
  		playGame: false,
  		isPanelOpen: false,
      actionParams: null
  	};
  }

  showPlayGame(show) {
  	this.setState({ playGame: show });
  }

  onExecuteAction(params) {
  	this.setState({ isPanelOpen: true, actionParams: params });
  }

  render () {
    return <div> 
    	<EventsBrowser showPlayGame={show => this.showPlayGame(show)} />
    	{<EventAction execute={params => this.onExecuteAction(params)}/>}
    	<SlidingPane
            className='slidingPanel'
            overlayClassName='some-custom-overlay-class'
            isOpen={ this.state.isPanelOpen }
            title='Account Information'
            subtitle=''
            onRequestClose={ () => {
                // triggered on "<" on left top click or on outside click
                this.setState({ isPanelOpen: false });
            } }>
            <AccountBalance />
        </SlidingPane>
    </div>;
  }
}

render(<App/>, document.getElementById('app'));