import React from 'react';
import {render} from 'react-dom';
import { Button, DropdownButton, MenuItem } from 'react-bootstrap';
import { playerAction } from './service';
import { reset } from './service';

import './eventAction.less';

export default class EventAction extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        action: '',
        amount: 0,
        platformCode: '',
        transactionId: '',
        played: true
    };
  }

  onChangeSettings(event, field) {
     this.setState({ [field]: event.target.value }); 
  }

  execute(e) {
     const actionPaylod = {
        action: this.state.action.toLowerCase(),
        amount: this.state.amount,
        platformCode: this.state.platformCode,
        TransactionID: this.state.transactionId
     };

     playerAction(actionPaylod).then(d => {
        this.props.execute();
        this.setState({ played: true });
     });
  }

  onSelectAction(e, o) {
      this.setState({ action: e });
  }

  renderForm(field,description) {
    return <form>
        <label>{`${field}:`}</label>  
        <input type="text" value={this.state[field]} onChange={e => this.onChangeSettings(e, field)}/>
		<div>{description}</div>
    </form>;
  }

  render () {
    let selectedAction = 'Actions';
    const { action } = this.state;
    if (action && action.length > 0) selectedAction = action;

    return <div className="eventAction">
      <div className="title">Event Action-玩家动作</div>
      <div className="forms">
        
        <DropdownButton title={selectedAction} bsSize="sm" id="dropdown-size-large" onSelect={(e, o) => this.onSelectAction(e, o)}>
            <MenuItem eventKey="Register">Register</MenuItem>
            <MenuItem eventKey="Email-Verify">Email-Verify</MenuItem>
            <MenuItem eventKey="Phone-Verify" >Phone-Verify</MenuItem>
            <MenuItem eventKey="ID-Verify">ID-Verify</MenuItem>
            <MenuItem eventKey="Deposit">Deposit</MenuItem>
            <MenuItem eventKey="Login">Login</MenuItem>
            <MenuItem eventKey="Settle">Settle</MenuItem>
        </DropdownButton>
        
        {this.renderForm("amount","金额")}
        {this.renderForm("platformCode","馆别")}
        {this.renderForm("transactionId","交易单号")}
        
        <div className="playWrap">
          <Button bsSize="sm" className="play" onClick={e => this.execute()}>Execute执行</Button>
          <Button bsSize="sm" className="play" onClick={e => this.props.execute()} disabled={!this.state.played}>查看玩家资讯</Button>
          <Button bsSize="sm" className="play" onClick={e => reset()} disabled={!this.state.played}>重置</Button>
        </div>
      </div>
    </div>;
  }
}