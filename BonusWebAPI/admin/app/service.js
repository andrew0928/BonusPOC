const api_endpoint = ''; //'http://172.19.3.65';

export function setBonus(setBonusModel) {
	return $.ajax({
		type: 'POST',
		url: api_endpoint + '/api/SetBonus',
		data: JSON.stringify(setBonusModel),
		contentType: "application/json; charset=utf-8",
        dataType: "json",
	});
}

export function getAccountInfo() {
	return $.ajax({
		type: 'POST',
		url: api_endpoint + '/api/getaccountinfo',
	});
}

export function playerAction(playerActionModel) {
	return $.ajax({
		type: 'POST',
		url: api_endpoint + '/api/PlayerAction',
		data: JSON.stringify(playerActionModel),
		contentType: "application/json; charset=utf-8",
        dataType: "json",
	});
}

export function getBonusLog() {
	return $.ajax({
		type: 'POST',
		url: api_endpoint + '/api/getbonuslog',
	});
}

export function getBonusStatusList() {
	return $.ajax({
		type: 'POST',
		url: api_endpoint + '/api/GetBonusStatusList',
	});
}

export function reset() {
	return $.ajax({
		type: 'POST',
		url: api_endpoint + '/api/Reset',
	});
}