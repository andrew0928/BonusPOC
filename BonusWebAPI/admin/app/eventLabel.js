import React from 'react';
import {render} from 'react-dom';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import classNames from 'classnames';

import './eventLabel.less';

export default class EventLabel extends React.Component {
  render () {
  	const classes = classNames('eventLabel', { selected: this.props.selected });

    return <div className={classes}>{this.props.content}</div>;
  }
}