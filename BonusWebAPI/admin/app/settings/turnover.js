import React from 'react';
import {render} from 'react-dom';

import EventVerification from '../eventVerification';

import './settingsForm.less';

export default class Turnover extends React.Component {
  constructor(props) {
  	super(props);

  	this.state = {
  		settings: {
        "EventClassType": "BoYu66.Services.Bonus.TurnoverBonusEvent",
			"EventTypeID": "turnoverBonus-PT",
			"RepeatCountLimit": 0,
			"BonusAmountTurnoverRate": 0.0066,
			"RequirePlatformCode": "PT",
			"WithdrawLimitRate": 1,
      rules: []
  		}
  	};

    // initialize settings on EventSettings
    props.onChangeSettings(this.state.settings);
  }

  onChangeSettings(event, field) {
  	 const { settings } = this.state;
  	 settings[field] = event.target.value;
  	 this.setState({ settings }); 

  	 this.props.onChangeSettings(settings);
  }

  renderForm(field, description) {
    return <form>
        <label>{`${field}:`}</label>  
        <input type="text" value={this.state.settings[field]} onChange={e => this.onChangeSettings(e, field)}/>
		<div>{description}</div>
    </form>;
  }

  render () {
  	return <div className="settingsForm">
		<p>流水红利，红利优先权低，当玩家下注特定馆别，并且无申请其他优先权较高红利时，触发此红利。</p>
		<p>例如：某玩家下注PT馆别，该玩家申请的其他红利无法使用PT流水冲销，系统将发放"玩家获得的PT流水*红利％数"的红利金额</p>
		{this.renderForm("EventTypeID", "红利代码，不同的红利使用不同的代码")}
	  	{this.renderForm("RepeatCountLimit","重复次数，该红利可重复领取的次数")}
		{this.renderForm("BonusAmountTurnoverRate","红利％数")}
  		{this.renderForm("RequirePlatformCode","所需投注馆别")}
	  	{this.renderForm("WithdrawLimitRate","提款限额倍数，红利金额乘该数值即为因该红利增加的提款限额")}


    <EventVerification showId={true} 
          showEmail={true} 
          showPhone={true} 
		  showDeposit={true}		  
          onChange={(e, field) => this.onChangeSettings(e, field)}/>
  	</div>;
  }
}