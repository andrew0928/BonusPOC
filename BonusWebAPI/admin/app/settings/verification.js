import React from 'react';
import {render} from 'react-dom';

import EventVerification from '../eventVerification';

import './settingsForm.less';

export default class Verification extends React.Component {
  constructor(props) {
  	super(props);
	
	//"EventTypeID": "event-idverify",
	//"EventClassType": "BoYu66.Services.Bonus.CompositeRulesEventBase",
  	this.state = {
  		settings: {
        "EventTypeID": "event-idverify",
        "EventClassType": "BoYu66.Services.Bonus.CompositeRulesEventBase",
  		"RepeatCountLimit": 1,
        "BonusAmount": 20,
        "WithdrawLimitRate": 5,
        "TurnoverLimitRate": 0,
        rules: []
  		}
  	};

    // initialize settings on EventSettings
    props.onChangeSettings(this.state.settings);
  }

  onChangeSettings(event, field) {
  	 const { settings } = this.state;
  	 settings[field] = event.target.value;
  	 this.setState({ settings }); 

  	 this.props.onChangeSettings(settings);
  }

  renderForm(field, description) {
    return <form>
        <label>{`${field}:`}</label>  
        <input type="text" value={this.state.settings[field]} onChange={e => this.onChangeSettings(e, field)}/>
		<div>{description}</div>
    </form>;
  }

  render () {
  	return <div className="settingsForm">
		<p>验证红利及其他通用红利，此红利可设定验证红利或其他通用红利</p>
		<p>例如：当玩家通过验证後，须打满特定流水即可获得设定的红利金额。又或某活动，设定玩家仅需打满特定流水，并通过特定验证，即可获得此红利金额。</p>
		{this.renderForm("EventTypeID", "红利代码，不同的红利使用不同的代码")}
  		{this.renderForm("BonusAmount","红利金额")}
	  	{this.renderForm("WithdrawLimitRate","提款限额倍数，红利金额乘该数值即为因该红利增加的提款限额")}
	  	{this.renderForm("TurnoverLimitRate","流水限制倍数，红利金额乘该数值即为该红利需打满的流水额度，打满该数值方可领取红利")}

    <EventVerification showId={true} 
          showEmail={true} 
          showPhone={true} 
		  showDeposit={true}
          onChange={(e, field) => this.onChangeSettings(e, field)}/>
  	</div>;
  }
}