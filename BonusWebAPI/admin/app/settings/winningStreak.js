import React from 'react';
import {render} from 'react-dom';

import EventVerification from '../eventVerification';

import './settingsForm.less';

export default class WinningStreak extends React.Component {
  constructor(props) {
  	super(props);
	
  	this.state = {
  		settings: {
		    "EventTypeID":"3WinningSteakBonus",
        "EventClassType": "BoYu66.Services.Bonus.WinningSteakTimesEvent",
  		  "RepeatCountLimit": 3,
        "BonusAmount": 300,
        "Priority": 1000,
        "MinTurnover": 5,
        "RequireWinningSteakTimes": 3,
        "RequirePlatformCode": "GB",
        "WithdrawLimitRate": 3,
        "TurnoverLimitRate": 3,
        rules: []
  		}
  	};

    // initialize settings on EventSettings
    props.onChangeSettings(this.state.settings);
  }

  onChangeSettings(event, field) {
  	 const { settings } = this.state;
  	 settings[field] = event.target.value;
  	 this.setState({ settings }); 

  	 this.props.onChangeSettings(settings);
  }

  renderForm(field, description) {
    return <form>
        <label>{`${field}:`}</label>
        <input type="text" value={this.state.settings[field]} onChange={e => this.onChangeSettings(e, field)}/>
		<div>{description}</div>
    </form>;
  }

  render () {
  	return <div className="settingsForm">
		<p>连赢红利，玩家投注特定馆别连续获胜达设定次数即可获得该红利。</p>
		<p>例如：GB体育三连赢红利。玩家连续三次派彩金额高於下注金额，即可获得该红利；若玩家连续下注，第一张注单派采金额高於下注金额（连赢次数计为1次），第二张注单派采金额低於下注金额（连赢次数重置，小计为0）。</p>
		{this.renderForm("EventTypeID", "红利代码，不同的红利使用不同的代码")}
	  	{this.renderForm("RepeatCountLimit","重复次数，该红利可重复领取的次数")}
  		{this.renderForm("BonusAmount","红利金额")}
	  	{this.renderForm("Priority","红利的优先顺序，数值越低代表优先程度越高")}
  		{this.renderForm("MinTurnover","最小启动流水")}
  		{this.renderForm("RequireWinningSteakTimes","所需连赢次数，当连赢次数达该数值方可触发此红利")}
  		{this.renderForm("RequirePlatformCode","所需投注馆别")}
	  	{this.renderForm("WithdrawLimitRate","提款限额倍数，红利金额乘该数值即为因该红利增加的提款限额")}
	  	{this.renderForm("TurnoverLimitRate","流水限制倍数，红利金额乘该数值即为该红利需打满的流水额度，打满该数值方可领取红利")}

		<p>領取資格</p>
         <EventVerification showId={true} 
          showEmail={true} 
          showPhone={true} 
		  showDeposit={true}		  
          onChange={(e, field) => this.onChangeSettings(e, field)}/>
  	</div>;
  }
}