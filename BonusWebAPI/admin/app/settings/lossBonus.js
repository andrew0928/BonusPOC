import React from 'react';
import {render} from 'react-dom';

import EventVerification from '../eventVerification';

import './settingsForm.less';

export default class LossBonus extends React.Component {
  constructor(props) {
  	super(props);
	
  	this.state = {
  		settings: {
        "EventClassType": "BoYu66.Services.Bonus.LossEvent",
  		  "EventTypeID": "LossBonus",
    		"RepeatCountLimit": 3,
        "BonusAmount": 500,
        "Priority": 1000,
        "MinTurnover": 10,
        "ReqireLossAmount": 10000,
        "RequirePlatformCode": "GB",
        "WithdrawLimitRate": 10,
        "TurnoverLimitRate": 0,
        rules: []
  		}
  	};

    // initialize settings on EventSettings
    props.onChangeSettings(this.state.settings);
  }

  onChangeSettings(event, field, description) {
  	 const { settings } = this.state;
  	 settings[field] = event.target.value;
  	 this.setState({ settings }); 

  	 this.props.onChangeSettings(settings);
  }

  renderForm(field, description) {
    return <form>
        <label>{`${field}:`}</label>
        <input type="text" value={this.state.settings[field]} onChange={e => this.onChangeSettings(e, field)}/>
		<div>{description}</div>
    </form>;
  }


  render () {
  	return <div className="settingsForm">
		<p>负营利，当玩家下注输赢为负数并累积到设定金额时，触发此红利。</p>
		<p>例如：玩家下注100派采5元，该笔注单输赢-95，由於输赢为负数，该笔注单为负营利，金额100，当玩家累积到设定金额时，即可触发此红利。</p>
		{this.renderForm("EventTypeID", "红利代码，不同的红利使用不同的代码")}
	  	{this.renderForm("RepeatCountLimit","重复次数，该红利可重复领取的次数")}
  		{this.renderForm("BonusAmount","红利金额")}
	  	{this.renderForm("Priority","红利的优先顺序，数值越低代表优先程度越高")}
  		{this.renderForm("MinTurnover","最小启动流水")}
  		{this.renderForm("ReqireLossAmount","所需负营利")}
  		{this.renderForm("RequirePlatformCode","所需投注馆别")}
	  	{this.renderForm("WithdrawLimitRate","提款限额倍数，红利金额乘该数值即为因该红利增加的提款限额")}
	  	{this.renderForm("TurnoverLimitRate","流水限制倍数，红利金额乘该数值即为该红利需打满的流水额度，打满该数值方可领取红利")}

      <EventVerification showId={true} 
          showEmail={true} 
          showPhone={true} 
 		  showDeposit={true}
          onChange={(e, field) => this.onChangeSettings(e, field)}/>
  	</div>;
  }
}