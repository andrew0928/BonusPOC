import React from 'react';
import {render} from 'react-dom';

import EventVerification from '../eventVerification';

import './settingsForm.less';

export default class Deposit extends React.Component {
  constructor(props) {
  	super(props);

  	this.state = {
  		settings: {
			  EventTypeID: "first-deposit", //首存一定要first-deposit，其他不用
        EventClassType: "BoYu66.Services.Bonus.DepositEvent",
  			MinDepositAmount: 1000,
	  		MaxBonusAmount: 2000,
	  		WithdrawLimitRate: 2,
	  		TurnoverLimitRate: 10,
			  RepeatCountLimit: 1,
			  BonusRate: 30,
			  Priority: 20,
        rules: []
  		}
  	};

    // initialize settings on EventSettings
    props.onChangeSettings(this.state.settings);
  }

  onChangeSettings(event, field) {
  	 const { settings } = this.state;
  	 settings[field] = event.target.value;
  	 this.setState({ settings }); 

  	 this.props.onChangeSettings(settings);
  }

  renderForm(field, description) {
    return <form>
        <label>{`${field}:`}</label>
        <input type="text" value={this.state.settings[field]} onChange={e => this.onChangeSettings(e, field)}/>
		<div>{description}</div>
    </form>;
  }


  render () {
  	return <div className="settingsForm">
		<p>存款红利，当玩家存款时所触发的红利，红利金额会因存款金额不同而有不同的红利金额</p>
		<p>例如：玩家存款1000，其红利金额为1000*BonusRate</p>
		{this.renderForm("EventTypeID", "红利代码，不同的红利使用不同的代码")}
  		{this.renderForm("MinDepositAmount","最小存款金额")}
	  	{this.renderForm("MaxBonusAmount","最大红利金额")}
	  	{this.renderForm("WithdrawLimitRate","提款限额倍数，红利金额乘该数值即为因该红利增加的提款限额")}
	  	{this.renderForm("TurnoverLimitRate","流水限制倍数，红利金额乘该数值即为该红利需打满的流水额度，打满该数值方可领取红利")}
	  	{this.renderForm("RepeatCountLimit","重复次数，该红利可重复领取的次数")}
	  	{this.renderForm("BonusRate","红利％数，红利金额为存款金额乘上该值")}
	  	{this.renderForm("Priority","红利的优先顺序，数值越低代表优先程度越高")}

      <EventVerification showId={true} 
          showEmail={true} 
          showPhone={true} 
          onChange={(e, field) => this.onChangeSettings(e, field)}/>
  	</div>;
  }
}