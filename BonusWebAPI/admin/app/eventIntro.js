import React from 'react';
import {render} from 'react-dom';
import { DropdownButton, MenuItem } from 'react-bootstrap';

import './eventIntro.less';

export default class EventIntro extends React.Component {
  render () {
    return <div className="eventIntro">{this.props.type}</div>;
  }
}