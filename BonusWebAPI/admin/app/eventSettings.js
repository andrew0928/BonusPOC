import React from 'react';
import {render} from 'react-dom';
import { Button } from 'react-bootstrap';

import Deposit from './settings/deposit';
import WinningStreak from './settings/winningStreak';
import LossBonus from './settings/lossBonus';
import LuckyTransfer from './settings/luckyTransfer';
import Turnover from './settings/turnover';
import Verification from './settings/verification';

import './eventSettings.less';


let getSettingsComponent = (event, callback) => {
	if (event === "deposit") return <Deposit onChangeSettings={settings => callback(settings)} />;
	if (event === "winningStreak") return <WinningStreak onChangeSettings={settings => callback(settings)} />;
	if (event === "lossBonus") return <LossBonus onChangeSettings={settings => callback(settings)} />;
  if (event === "luckyTransfer") return <LuckyTransfer onChangeSettings={settings => callback(settings)} />;
  if (event === "turnover") return <Turnover onChangeSettings={settings => callback(settings)} />;
  if (event === "verification") return <Verification onChangeSettings={settings => callback(settings)} />;
};

export default class EventSettings extends React.Component {
  constructor(props) {
  	super(props);

  	this.state = {
  		settings: null
  	};
  }

  onChangeSettings (settings) { 
  	this.setState({ settings });
  }

  render () {
  	return <div className="eventSettings">
  		{getSettingsComponent(this.props.type, settings => this.onChangeSettings(settings))}

  		<Button bsSize="sm" className="add" onClick={e => {this.props.onAddEventSettings(this.state.settings)}}>Add</Button>
  	</div>;
  }
}