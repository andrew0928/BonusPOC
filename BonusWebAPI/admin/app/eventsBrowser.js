import React from 'react';
import {render} from 'react-dom';
import { Grid, Col, Row } from 'react-bootstrap';

import EventLabel from './eventLabel';
import EventModule from './eventModule';
import { setBonus } from './service';

import './eventsBrowser.less';

const events = ['deposit', 'winningStreak', 'lossBonus', 'luckyTransfer', 'turnover', 'verification' ];
const eventZhNames = ['存款', '連贏', '負營利', '幸運數字', '流水紅利', '驗證紅利'];

export default class EventsBrowser extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
        selectedEvent: null,
        settings: null,
        addedEvent: null
    };
  }

  onSelectEvent(e, selected) {
    this.setState({ selectedEvent: selected, settings: null }, () => this.props.showPlayGame(false));
  }

  onAddEventSettings(settings) {
     const eventIdx = events.indexOf(this.state.selectedEvent);
     setBonus(settings).then(() => this.setState({ settings , selectedEvent: null, addedEvent: eventZhNames[eventIdx] }))
        .fail(e => alert("failed to set bonus..."));
    this.setState({ settings , selectedEvent: null, addedEvent: eventZhNames[eventIdx] }, () => this.props.showPlayGame(true));
  }

  renderEventLabel(eventIndex) {
    const { selectedEvent } = this.state;

    return <Col xs={6} sm={4} md={2} lg={2} onClick={e => this.onSelectEvent(e, events[eventIndex])} >
        <EventLabel content={eventZhNames[eventIndex]} selected={selectedEvent === events[eventIndex]} />
    </Col>;
  }

  render () {
    const { selectedEvent, settings, addedEvent } = this.state;

    return <div className="eventsBrowser">
    	<Grid className="eventsContainer">
    		<Row>
    			{this.renderEventLabel(0)}
                {this.renderEventLabel(1)}
    			
                <Col xs={12} xsHidden={events.indexOf(selectedEvent) > 1} smHidden={true} mdHidden={true} lgHidden={true}>
                    <EventModule type={selectedEvent} onAddEventSettings={settings => this.onAddEventSettings(settings)} />
                </Col>
    			
                {this.renderEventLabel(2)}

                <Col xsHidden={true} sm={12} smHidden={(events.indexOf(selectedEvent) > 2)} mdHidden={true} lgHidden={true}>
                    <EventModule type={selectedEvent} onAddEventSettings={settings => this.onAddEventSettings(settings)} />
                </Col>

                {this.renderEventLabel(3)}
                
                <Col xs={12} xsHidden={events.indexOf(selectedEvent) < 2 || events.indexOf(selectedEvent) > 3} smHidden={true} mdHidden={true} lgHidden={true}>
                    <EventModule type={selectedEvent} onAddEventSettings={settings => this.onAddEventSettings(settings)}/>
                </Col>


                {this.renderEventLabel(4)}
                {this.renderEventLabel(5)}

                <Col xs={12} xsHidden={events.indexOf(selectedEvent) < 4} smHidden={true} mdHidden={true} lgHidden={true}>
                    <EventModule type={selectedEvent} onAddEventSettings={settings => this.onAddEventSettings(settings)} />
                </Col>

                <Col xsHidden={true} sm={12} smHidden={events.indexOf(selectedEvent) < 3} mdHidden={true} lgHidden={true}>
                    <EventModule type={this.state.selectedEvent} onAddEventSettings={settings => this.onAddEventSettings(settings)}/>
                </Col>

                <Col xsHidden={true} smHidden={true} smHidden={true} md={12} lg={12}>
                    <EventModule type={selectedEvent} onAddEventSettings={settings => this.onAddEventSettings(settings)}/>
                </Col>
    		</Row>
            {settings && <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <div className="addedSettings">
                        <label>{addedEvent}</label>
                        {Object.keys(settings).map(s => <div className="eventFactor" key={s}>{`${s}: ${settings[s]}`}</div>)}
                    </div>
                </Col>
            </Row>}
    	</Grid>
    </div>;
  }
}