import React from 'react';
import {render} from 'react-dom';
import { Table, Panel } from 'react-bootstrap';

import { getAccountInfo, getBonusLog, getBonusStatusList } from './service';

import './accountBalance.less';

export default class AccountBalance extends React.Component {
  constructor (props) {
  	 super(props);
  	 this.state = { 
        accountInfo: null, 
        bonusLogs: [],
        registeredRules: [],
        availableRules:[],
        readyCompleteRules:[]
      };
  }

  componentWillMount() {
  	getAccountInfo().then(accountInfo => this.setState({ accountInfo }));
    getBonusLog().then(bonusLogs => this.setState({ bonusLogs }));
    getBonusStatusList().then(data => {
        this.setState({
            registeredRules: data.registered_rules,
            availableRules: data.available_rules,
            readyCompleteRules: data.ready_complete_rules
        });
    });
  }

  render () {
  	const { accountInfo, bonusLogs, availableRules, 
      registeredRules, readyCompleteRules } = this.state;
  	if (!accountInfo) return null;

    return <div className="accoutBalance">
    	<Table>
        <thead>
          <tr>
            {Object.keys(accountInfo).map(a => <th key={a}>{a}</th>)}
          </tr>
        </thead>
        <tbody>
          <tr>
            {Object.keys(accountInfo).map(a => <th key={a}>{accountInfo[a]}</th>)}
          </tr>
        </tbody>
      </Table>

      <Panel header="Available Rules" bsStyle="info">
          {availableRules.map((rule, idx) => <div key={idx}>{rule}</div>)}
      </Panel>

      <Panel header="Registered Rules" bsStyle="info">
          {registeredRules.map((rule, idx) => <div key={idx}>{rule}</div>)}
      </Panel>

      <Panel header="Achieved Rules" bsStyle="info">
          {readyCompleteRules.map((rule, idx) => <div key={idx}>{rule}</div>)}
      </Panel>

      <Panel header="Bonus Logs" bsStyle="info">
          {bonusLogs.map((log, idx) => <div key={idx}>{log}</div>)}
      </Panel>
    </div>;
  }
}