import React from 'react';
import {render} from 'react-dom';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import './eventVerification.less';

const verifications = {
	"id-verify": {
		"RuleType": "BoYu66.Services.Bonus.UserVerifyRule",
      	"VerifyType": "id-verify",
    },
    "email-verify": {
    	"RuleType": "BoYu66.Services.Bonus.UserVerifyRule",
      	"VerifyType": "email-verify",
    },
    "phone-verify": {
    	"RuleType": "BoYu66.Services.Bonus.UserVerifyRule",
      	"VerifyType": "phone-verify"
    },
	"deposit": {
    	"RuleType": "BoYu66.Services.Bonus.DepositRule",
      	"MinDepositAmount": 100,
		"VerifyType": "deposit"		

    }
};

export default class EventVerification extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			value: null
		};
	}

	onSelect(value) {
		this.setState({ value }, () => {
			let e = {
				target: {
					value: this.state.value.map(v => verifications[v.value])
				}
			};
			this.props.onChange(e, 'rules');
		});
	}

  render () {
  	const { showId, showEmail, showPhone , showDeposit } = this.props;

    let options = [];

    if (showId) options.push({
    	value: verifications["id-verify"].VerifyType,
    	label: 'ID'
    });
    if (showEmail) options.push({
    	value: verifications["email-verify"].VerifyType,
    	label: 'email'
    });
    if (showPhone) options.push({
    	value: verifications["phone-verify"].VerifyType,
    	label: 'phone'
    });
    if (showDeposit) options.push({
    	value: verifications["deposit"].VerifyType,
    	label: 'deposit'
    });
    return <div className="eventVerification">
    	<Select name=""
    		placeholder="Select verification method"
    		options={options}
    		onChange={val => this.onSelect(val)}
    		value={this.state.value}
    		multi />
    </div>;
  }
}