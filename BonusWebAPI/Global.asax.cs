﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace BonusWebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //HttpContext.Current.Response.SetCookie(new HttpCookie("userid", Guid.NewGuid().ToString()));
            GlobalConfiguration.Configure(WebApiConfig.Register);
            System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }
    }
}
