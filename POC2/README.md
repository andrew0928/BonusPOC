# Bonus / Billing Service POC 設計目標

這份 POC 是要證明這樣的 code 結構能解決下列問題:

1. Billing Service 能提供獨立的 API 供外界呼叫，同時能透過 Event Driven 系統跟其他服務搭配，解決秒返等等即時回應的問題。
1. Bonus Service 能透過 Event Subscription 機制來回應各種紅利活動的處理
1. Bonus Event 紅利活動的設計，能夠再不重新啟動及更新 Billing / Bonus Engine 的前提下，動態的擴充與新增 (如一般常見的 plugins 擴充機制)

同時也確認這樣的機制能正確的處理既有的各種紅利條件設計:

1. 首存紅利
1. 再存紅利
1. 手動及自動領取
1. (TBD)


正式的架構，將會這樣處理:

1. Billing Engine 負責例行的帳務處理，包含 deposit / withdraw, transfer in / out, bet / settle, bonus (in)
2. Billing Engine 會觸發對應的 Event, 包含 CashEvent, BetEvent, SettleEvent, 驅動其他系統 (如 Bonus Service) 做對應的動作。
3. 事件驅動機制統一透過 RabbitMQ, 採用 publish / subscribtion 機制 (一對多) 的架構進行
4. 其他系統 (如 Bonus), 透過訂閱 Billing Engine  的事件，來處理各種紅利觸發及執行的過程。
5. 紅利系統 (Bonus) 的各種活動，採用 Plugins 的模式，不需要調整主系統，也不需要停機，就能動態的擴充各種不同類型及規則的紅利活動





![](classdiagram1.png)
> class diagram
