﻿using BoYu66.Services.Billing;
using BoYu66.Services.Bonus;
using BoYu66.Services.Common;
using BoYu66.Services.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Init Billing Engine
            BillingEngine.Init(new BillingEngine());

            // Init Membership Engine
            MemberEngine.Init(new MemberEngine());

            // Init Bonus Engine
            BonusEngine.Init(new BonusEngine());

            //
            // print headlines
            //
            {
                ActionInfo act = new ActionInfo();
                Console.WriteLine(act.GetHeaderLine() + " | " + BillingEngine.Current.CurrentAccountInfo.GetHeaderLine());
                Console.WriteLine(act.GetSplitLine() + " | " + BillingEngine.Current.CurrentAccountInfo.GetSplitLine());
                Console.WriteLine("--                   --         | " + BillingEngine.Current.CurrentAccountInfo.GetDataLine());
            }


            // 自動領取驗證紅利
            BonusEngine.Current.RuleCompleted += (rule) =>
            {
                //Console.WriteLine("                                                                              # Event: {0} 已可領取紅利", rule);
                //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{rule}] 領取紅利: {rule.BonusAmount}");
                rule.Complete(BillingEngine.Current, BonusEngine.Current);
            };

            //設定紅利
            BonusRuleModel firstDepositBonus = new BonusRuleModel { EventTypeID = "first-deposit", BonusAmount = 0, WithdrawLimitRate = 2, TurnoverLimitRate = 10, BonusRate = 30, MaxBonusAmount = 2000, MinDepositAmount = 100 };
            BonusRuleModel depositBonus = new BonusRuleModel { EventTypeID = "deposit", BonusAmount = 0, WithdrawLimitRate = 1, TurnoverLimitRate = 5, BonusRate = 10, MaxBonusAmount = 2000, MinDepositAmount = 100 };
            BonusRuleModel idVerifyBonus = new BonusRuleModel { BonusAmount = 20 , WithdrawLimitRate = 5 , TurnoverLimitRate = 0 };
            BonusRuleModel emailVerifyBonus = new BonusRuleModel { BonusAmount = 10 , WithdrawLimitRate = 5 , TurnoverLimitRate = 0 };
            BonusRuleModel phoneVerifyBonus = new BonusRuleModel { BonusAmount = 18 , WithdrawLimitRate = 5 , TurnoverLimitRate = 0 };

            BonusRuleModel winningSteakBonus = new BonusRuleModel { Priority = 1000, BonusAmount = 300 , WithdrawLimitRate = 3 , MinTurnover = 5 , RequireWinningSteakTimes = 3 , RequirePlatformCode = "GB" , TurnoverLimitRate = 3};
            BonusRuleModel lossBonus = new BonusRuleModel { Priority = 1001, BonusAmount = 500 , WithdrawLimitRate = 10 , MinTurnover = 10 , ReqireLossAmount = 10000, RequirePlatformCode = "GB" };
            BonusRuleModel luckyTransBonus = new BonusRuleModel { Luckystr = "66", BonusAmount = 50 , WithdrawLimitRate = 3 , TurnoverLimitRate =  10, RequirePlatformCode = "GD" };
            BonusRuleModel turnoverBonus = new BonusRuleModel { MinTurnover = 10, BonusAmountTurnoverRate = 0.0066 , WithdrawLimitRate = 1 , RequirePlatformCode = "PT" };


            //取主帳戶相關資訊
            var actualBalance = BillingEngine.Current.CurrentAccountInfo.ActualBalance;
            var log = BonusEngine.Current.GetLogString();


            // -----------------------------------------------------------------------------------------------------------------
            //  start user interactive sessions
            // -----------------------------------------------------------------------------------------------------------------




            // 建立新帳號, 全新的主帳戶紀錄
            MemberEngine.Current.RegisterAccount(firstDepositBonus, idVerifyBonus, emailVerifyBonus, phoneVerifyBonus, BillingEngine.Current, BonusEngine.Current);

            MemberEngine.Current.VerifyAccount("id-verify", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Deposit(1000.00M, BillingEngine.Current, BonusEngine.Current);

            MemberEngine.Current.Login();

            // 使用者行為: 通過 PHONE 驗證
            MemberEngine.Current.VerifyAccount("phone-verify", BillingEngine.Current, BonusEngine.Current);
            //BonusEngine.Current.RegisterEvent(new CompositeRulesEventBase("HI",2,30,2,1));


            // 使用者行為: 通過 EMAIL 驗證
            MemberEngine.Current.VerifyAccount("email-verify", BillingEngine.Current, BonusEngine.Current);

            BillingEngine.Current.Settle(null, 100, 100, true, true, true, "BSG", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle(null, 100, 100, true, true, true, "BSG", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle(null, 100, 100, true, true, true, "BSG", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle(null, 100, 100, true, true, true, "BSG", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle(null, 100, 100, true, true, true, "BSG", BillingEngine.Current, BonusEngine.Current);

            BonusEngine.Current.RegisterEvent(new WinningSteakTimesEvent(winningSteakBonus)
            {
                MatchRules = new List<RuleBase>()
                        {
                            new UserVerifyRule() { VerifyType = "email-verify" },
                            new DepositRule() { MinDepositAmount = 100 }
                        }
            });

            //var aav = new LossEvent(null);
            //aav.MatchRules = new List<RuleBase>()
            //            {
            //                new UserVerifyRule() { VerifyType = "email-verify" },
            //                new DepositRule() { MinDepositAmount = 100 }
            //            };
            //BonusEngine.Current.RegisterEvent((EventBase)aav);

            BonusEngine.Current.RegisterEvent(new LossEvent(lossBonus));
            BonusEngine.Current.RegisterEvent(new LuckyTransactionEvent(luckyTransBonus));
            BonusEngine.Current.RegisterEvent(new TurnoverBonusEvent("PT"));

            // 使用者行為: (三連贏)
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 100, 100, true, true, true, "GB", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 100, 100, true, true, true, "GB", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 100, 100, true, true, true, "GB", BillingEngine.Current, BonusEngine.Current);


            BillingEngine.Current.Settle("TID - 25d68a5d13044c1aab5e93848000d166", 100, 100, true, true, true, "GD", BillingEngine.Current, BonusEngine.Current);
            //BonusEngine.Current.RegisterEvent(new LuckyTransactionEvent("66"));
            //BillingEngine.Current.Settle("TID - 25d68a5d13044c1aab5e938480006266", 1000, 1000, true, true, true, "GD");
            //BonusEngine.Current.RegisterEvent(new LuckyTransactionEvent("66"));
            //BillingEngine.Current.Settle("TID - 25d68a5d13044c1aab5e938480006366", 1000, 1000, true, true, true, "GD");
            //BonusEngine.Current.RegisterEvent(new LuckyTransactionEvent("66"));
            //BillingEngine.Current.Settle("TID - 25d68a5d13044c1aab5e938480006466", 1000, 1000, true, true, true, "GD");

            //BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 100, 100, true, true, true, "GB");
            //BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 1000, 1000, true, true, true, "GB");

            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 3000, 3000, true, true, true, "PT", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", 3000, 3000, true, true, true, "PT", BillingEngine.Current, BonusEngine.Current);

            BonusEngine.Current.RegisterEvent(new DepositEvent(depositBonus));

            BillingEngine.Current.Deposit(1000.00M, BillingEngine.Current, BonusEngine.Current);

            //log = BonusEngine.Current.GetLogString();

            //復活金-負盈利1000
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", -800, 800, true, true, true, "GB", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", -300, 300, true, true, true, "GB", BillingEngine.Current, BonusEngine.Current);
            BillingEngine.Current.Settle($"TID-{Guid.NewGuid()}", -9000, 9000, true, true, true, "GB", BillingEngine.Current, BonusEngine.Current);

        }
    }























}
