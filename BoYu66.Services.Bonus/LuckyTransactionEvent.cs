﻿using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    /// <summary>
    /// 機會中獎紅利
    /// </summary>
    public class LuckyTransactionEvent : CompositeRulesEventBase
    {
        /// <summary>
        /// 幸運單號
        /// </summary>
        public readonly string Luckystr;
        public readonly string RequirePlatformCode = "";   //所需投注代碼


        public LuckyTransactionEvent(string luckystr)
        {
            //this.Luckystr = luckystr;
            //this.BonusAmount = 36;
            //this.WithdrawLimitRate = 3;
            //this.TurnoverLimitRate = 10;

            this.MatchRules.Add(new LuckyTransactionRule() { Luckystr = luckystr});
            //this.MatchRules.Add(new CountRule() { LimitCounts = 3});

            //this.Priority = 99990;
        }

        public LuckyTransactionEvent(BonusRuleModel bonusSetting)
        {
            this.Luckystr = bonusSetting.Luckystr;
            this.BonusAmount = bonusSetting.BonusAmount;
            this.WithdrawLimitRate = bonusSetting.WithdrawLimitRate;
            this.TurnoverLimitRate = bonusSetting.TurnoverLimitRate;
            this.RequirePlatformCode = bonusSetting.RequirePlatformCode;
            this.RepeatCountLimit = bonusSetting.RepeatCountLimit;

            this.MatchRules.Add(new LuckyTransactionRule() { Luckystr = this.Luckystr , RequirePlatformCode = this.RequirePlatformCode });
            //this.MatchRules.Add(new CountRule() { LimitCounts = 3});

            //this.Priority = 99990;
        }

        public override string ToString()
        {
            return $"尾碼{this.Luckystr}幸運注單";
        }
    }
}
