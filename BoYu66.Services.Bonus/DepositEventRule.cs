﻿using BoYu66.Services.Billing;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    public class DepositEvent : CompositeRulesEventBase
    {
        public readonly decimal MinDepositAmount = 20;

        public DepositEvent()
        {
            this.BonusAmount = 0;
            this.WithdrawLimitRate = 1;
            this.TurnoverLimitRate = 10;
            this.BonusRate = 10;
            this.MaxBonusAmount = 2000;

            this.MatchRules.Add(new DepositRule() { MinDepositAmount = 100 });
            //this.MatchRules.Add(new FridayLoginRule() { });
            //次數限制 區間限制
            //首存次數只有一次，不能重複申請
            //存款紅利可設定次數 區間
            //this.MatchRules.Add(new CountRule() { LimitCounts = 3 });

        }

        public DepositEvent(BonusRuleModel bonusSetting)
        {
            this.EventTypeID = bonusSetting.EventTypeID;
            this.BonusAmount = bonusSetting.BonusAmount;
            this.WithdrawLimitRate = bonusSetting.WithdrawLimitRate;
            this.TurnoverLimitRate = bonusSetting.TurnoverLimitRate;
            this.BonusRate = bonusSetting.BonusRate;
            this.MaxBonusAmount = bonusSetting.MaxBonusAmount;
            this.RepeatCountLimit = bonusSetting.RepeatCountLimit;
            this.MatchRules.Add(new DepositRule() { MinDepositAmount = bonusSetting.MinDepositAmount });
            //this.MatchRules.Add(new FridayLoginRule() { });
            //次數限制 區間限制
            //首存次數只有一次，不能重複申請
            //存款紅利可設定次數 區間
            //this.MatchRules.Add(new CountRule() { LimitCounts = 3 });

        }


        public override bool IsMatchAndInit(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            if ((action.Type == "first-deposit"|| action.Type == "deposit") && this.BonusRate != 0 && action.Billing.Amount >= this.MinDepositAmount)
            {
                decimal tmpBonusAmount = (action.Billing.Amount * (this.BonusRate / 100));
                this.BonusAmount = tmpBonusAmount >= this.MaxBonusAmount ? this.MaxBonusAmount : tmpBonusAmount;
            }
            return base.IsMatchAndInit(action , billingEngine , bonusEngine);
        }

        public override string ToString()
        {
            return "用戶存款紅利";
        }
    }

    //[Obsolete]
    //public class DepositEventRule : SettleEventBase
    //{
    //    public readonly decimal MinDepositAmount = 1888;

    //    public DepositEventRule()
    //    {
    //        this.MinDepositAmount = 100; //未訂
    //        //this.BonusAmount = 168;
    //        this.WithdrawLimitRate = 1; //未訂
    //        this.TurnoverLimitRate = 10;
    //        this.BonusRate = 10;    //New
    //        this.MaxBonusAmount = 1000; //New
    //    }

    //    public override void ApplyAction(ActionInfo action)
    //    {
    //        base.ApplyAction(action);
    //        // 不能 complete
    //        //this.IsReadyForComplete = false;
    //    }

    //    public override bool IsMatchAndInit(ActionInfo action)
    //    {
    //        if (action.Type == "deposit" && action.Billing.Amount >= this.MinDepositAmount)
    //        {
    //            decimal tmpBonusAmount = (action.Billing.Amount * (this.BonusRate / 100));
    //            this.BonusAmount = tmpBonusAmount >= this.MaxBonusAmount ? this.MaxBonusAmount : tmpBonusAmount;
    //            return base.IsMatchAndInit(action);
    //        }
    //        return false;
    //    }

    //    public override string ToString()
    //    {
    //        return $"新用戶存款紅利，存滿{this.MinDepositAmount}以上送{this.BonusRate}%紅利金，最高上限{this.MaxBonusAmount}紅利金";

    //    }

    //}
}
