﻿using BoYu66.Services.Billing;
using BoYu66.Services.Common;
using BoYu66.Services.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{

    public class BonusEngine : EngineBase<BonusEngine>
    {
        public delegate void RuleCompleteHandler(EventBase rule);
        public delegate void CompleteBonusHandler();

        public event RuleCompleteHandler RuleCompleted = (rule) => { };
        public event CompleteBonusHandler CompleteBonus = () => { };

        public static StringBuilder LogString = new StringBuilder();
        public static List<string> LogList = new List<string>();

        // 建立可自動申請的活動清單
        private List<EventBase> available_rules = new List<EventBase>()
        {
            //new FirstDepositEventRule(),
            //new IdVerifyEvent(),
            //new PhoneVerifyEvent(),
            //new EmailVerifyEvent()
        };

        // 建立已申請的紅利活動清單
        private List<EventBase> registered_rules = new List<EventBase>()
        {
        };

        // 建立以達成領取紅利條件的活動清單
        private List<EventBase> ready_complete_rules = new List<EventBase>();

        private Dictionary<string, int> completeBonus = new Dictionary<string, int>();

        public Dictionary<string,List<string>> GetBonusStatusList()
        {
            var availableRules = new List<string>();
            var registeredRules = new List<string>();
            var readyCompleteRules = new List<string>();
            foreach(var a in available_rules) { availableRules.Add(a.ToString()); }
            foreach(var b in registered_rules) { registeredRules.Add(b.ToString()); }
            foreach(var c in ready_complete_rules) { readyCompleteRules.Add(c.ToString()); }
            return new Dictionary<string, List<string>>()
            {
                { "available_rules", availableRules},
                { "registered_rules", registeredRules},
                { "ready_complete_rules", readyCompleteRules},
            };
        }

        //private BillingEngineBase _billing_engine = null;


        //private Dictionary<string, int> _events_complete_count = new Dictionary<string, int>();

        internal int GetEventCompleteCount(string eventID)
        {
            if (completeBonus.ContainsKey(eventID) == false) completeBonus[eventID] = 0;

            return completeBonus[eventID];
        }
        internal int IncrementEventCompleteCount(string eventID)
        {
            if (completeBonus.ContainsKey(eventID) == false) completeBonus[eventID] = 0;

            return ++completeBonus[eventID];
        }




        public BonusEngine() : this(BillingEngine.Current, MemberEngine.Current)
        {

        }

        public BonusEngine(BillingEngine billing_engine, MemberEngine member_engine)
        {
            billing_engine.CashEvent += BillingEventHandler;
            billing_engine.SettleEvent += BillingEventHandler;
            //billing_engine.BonusEvent += BillingEventHandler;

            //this._billing_engine = billing_engine;

            member_engine.RegUserEvent += BillingEventHandler;
            member_engine.VerifyEvent += BillingEventHandler;
            member_engine.UserLoginEvent += BillingEventHandler;
        }


        public void RegisterEvent(EventBase rule)
        {
            if (rule.RepeatCountLimit == 0 || rule.RepeatCountLimit > this.GetEventCompleteCount(rule.EventTypeID))
            {
                this.IncrementEventCompleteCount(rule.EventTypeID);
                this.InsertRulesByPriority(rule, this.available_rules);
                Log($"註冊活動: {rule}, {rule.EventTypeID} 成功，已加入每次事件的資格確認清單。");
            }
            else
            {
                // 超出限制
                Log($"註冊活動: {rule}, {rule.EventTypeID} 註冊次數({this.GetEventCompleteCount(rule.EventTypeID)}) 已經超過限制次數({rule.RepeatCountLimit})。無法註冊活動");
            }
        }


        private void InsertRulesByPriority(EventBase rule, List<EventBase> list)
        {
            for (int index = 0; index < list.Count; index++)
            {
                if (rule.Priority < list[index].Priority)
                {
                    list.Insert(index, rule);
                    return;
                }
            }

            list.Add(rule);
        }



        public void CompleteEvent(EventBase rule , BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            if (rule.IsReadyForComplete == false) return;
            rule.Complete(billingEngine , bonusEngine);
            this.ready_complete_rules.Remove(rule);
            

        }


        private void BillingEventHandler(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            //
            // scan available_rules, 若有 match 則自動申請該紅利活動
            //
            List<EventBase> _temp = new List<EventBase>();
            foreach (EventBase rule in this.available_rules)
            {
                if (rule.IsMatchAndInit(action, billingEngine, bonusEngine) == false) continue;
                this.InsertRulesByPriority(rule, this.registered_rules);
                _temp.Add(rule);
            }
            foreach(EventBase _removed_rule in _temp)
            {
                this.available_rules.Remove(_removed_rule);
            }


            //
            // scan registered_rules, 若條件符合則更新紅利活動的進度。若進度已經完成，則會將狀態改為 IsReadyForComplete => true, 同時觸發 RuleCompleted Event
            //
            _temp.Clear();

            //if (action.Turnover != null && action.Turnover.Amount > 0 && BillingEngine.Current.CurrentAccountInfo.TurnoverAmount < 0)
            //{
            //    decimal diff = Math.Min(action.Turnover.Amount, BillingEngine.Current.CurrentAccountInfo.TurnoverAmount * -1);
            //    action.Turnover.Amount -= diff;
            //    BillingEngine.Current.CurrentAccountInfo.TurnoverAmount += diff;
            //}

            foreach (EventBase rule in this.registered_rules)
            {
                rule.ApplyAction(action , billingEngine, bonusEngine);
                if (rule.IsReadyForComplete == true)
                {
                    this.RuleCompleted(rule);
                    this.ready_complete_rules.Add(rule);
                    _temp.Add(rule);
                }
            }
            foreach(EventBase _ready_complete_rule in _temp)
            {
                //CompleteBonus
                this.registered_rules.Remove(_ready_complete_rule);
                //int count = 0;
                //if (completeBonus.TryGetValue(_ready_complete_rule.ToString(), out count))
                //{
                //    completeBonus[_ready_complete_rule.ToString()]++;
                //}
                //else
                //{
                //    completeBonus.Add(_ready_complete_rule.ToString(), 1);
                //}
                //this.CompleteBonus(completeBonus);
                this.CompleteBonus();

            }
        }

        //protected override void Action(ActionInfo action )
        //{
        //    throw new NotImplementedException();
        //}




        public static void Log()
        {
            Console.WriteLine();
        }

        public static void Log(string message)
        {
            LogString.AppendLine("# " + message);
            LogList.Add(message);
            Console.WriteLine(string.Empty.PadRight(78) + "# " + message);
        }

        public StringBuilder GetLogString()
        {
            return LogString;
        }

        public List<string> GetLogList()
        {
            var resultList = new List<string>();
            LogList.ForEach(x => resultList.Add(x));
            LogList.Clear();
            return resultList;
        }

        public void AddLogList(string msg)
        {
            LogList.Add(msg);
        }

        protected override void Action(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            throw new NotImplementedException();
        }
    }
}
