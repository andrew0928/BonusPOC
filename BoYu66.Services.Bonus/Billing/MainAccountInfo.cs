﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Billing
{

    public class MainAccountInfo
    {
        /// <summary>
        /// 實際餘額 (主帳戶，不包含紅利錢包，指真正能提領出來的部分)
        /// </summary>
        public decimal ActualBalance;

        /// <summary>
        /// 紅利錢包，派發的紅利(先發紅利)若只能用於下注，還不能提款則必須歸于紅利錢包
        /// (待確認: 那些情況下，紅利錢包的部分可以轉移到主帳戶?)
        /// </summary>
        public decimal BonusWallet;

        /// <summary>
        /// 提款限額 (定義: 提款時，必須確認主帳戶的餘額不得低於提款限額)
        /// </summary>
        public decimal WithdrawLimit;

        /// <summary>
        /// 所需流水
        /// </summary>
        public decimal TurnoverAmount;


        public string GetHeaderLine()
        {
            return string.Format("主帳戶餘額 紅利錢包   提款限額   有效流水   ");
        }

        public string GetSplitLine()
        {
            return string.Format("---------- ---------- ---------- ----------");
        }

        public string GetDataLine()
        {
            return string.Format(
                "{0,10} {1,10} {2,10} {3,10}",
                this.ActualBalance,
                this.BonusWallet,
                this.WithdrawLimit,
                this.TurnoverAmount);
        }
    }
}
