﻿using BoYu66.Services.Bonus;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Billing
{
    

    /// <summary>
    /// Billing Engine 必須是 stateless
    /// </summary>
    public class BillingEngine : EngineBase<BillingEngine>
    {
        public event ActionEventHandler CashEvent = (action , billingEngine, bonusEngine) => { };
        public event ActionEventHandler BetEvent = (action, billingEngine, bonusEngine) => { };
        public event ActionEventHandler SettleEvent = (action, billingEngine, bonusEngine) => { };


        private MainAccountInfo _dummy_account_info = new MainAccountInfo();
        public MainAccountInfo CurrentAccountInfo
        {
            get
            {
                // TODO: 應該要從目前的 context 取得 account id, 並且傳回代表他的主帳戶 entity
                return _dummy_account_info;
            }
        }




        protected override void Action(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            //
            // doing: billing compute engine part
            //

            switch (action.Type)
            {
                case "first-deposit":
                case "deposit":
                    billingEngine.CurrentAccountInfo.ActualBalance += action.Billing.Amount;
                    billingEngine.CurrentAccountInfo.WithdrawLimit += action.Billing.WithdrawLimitAmount;
                    
                    this.CashEvent(action , billingEngine , bonusEngine);
                    break;

                case "bonus":
                    billingEngine.CurrentAccountInfo.ActualBalance += action.Billing.Amount;
                    billingEngine.CurrentAccountInfo.WithdrawLimit += action.Billing.WithdrawLimitAmount;

                    // do NOT fire any event here
                    break;

                case "withdraw":

                    this.CashEvent(action, billingEngine, bonusEngine);
                    break;


                case "bet":

                    this.BetEvent(action, billingEngine, bonusEngine);
                    break;

                case "settle":
                    billingEngine.CurrentAccountInfo.ActualBalance += action.Billing.Amount;
                    billingEngine.CurrentAccountInfo.WithdrawLimit = Math.Max(billingEngine.CurrentAccountInfo.WithdrawLimit - action.Turnover.Amount, 0);

                    this.SettleEvent(action, billingEngine, bonusEngine);

                    //BillingEngine.Current.CurrentAccountInfo.TurnoverAmount += action.Turnover.Amount;
                    action.Turnover.Amount = 0;

                    break;
            }

            if (action.Type != "bonus")
            {
                Console.WriteLine(action.GetDataLine() + " | " + billingEngine.CurrentAccountInfo.GetDataLine());
                bonusEngine.AddLogList(action.Type + " " + action.Billing.Amount.ToString());
            }
        }


        private int _deposit_count = 0;

        public void Deposit(decimal amount , BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            this.Action(new ActionInfo()
            {
                Type = (_deposit_count++ == 0)?"first-deposit":"deposit",
                Billing = new BillingAction()
                {
                    Amount = amount,
                    WithdrawLimitAmount = amount
                }
            }, billingEngine , bonusEngine);
        }

        public void Withdraw() { }

        public void Bet() {
        }

        public void Settle(string transactionId, decimal winlose, decimal turnover, bool isBonus, bool isEffectTurnover, bool isTurnoverBonus , string platformCode, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            this.Action(new ActionInfo()
            {
                Type = "settle",
                
                Billing = new BillingAction()
                {
                    Amount = winlose,
                    TransactionId = transactionId
                },
                Turnover = new TurnoverAction()
                {
                    //BetAmount = 100,
                    Amount = turnover,
                    PlatformCode = platformCode,
                    ForBonusEvent = isBonus,
                    ForEffectTurnoverAmount = isEffectTurnover,
                    ForTurnoverBonus = isTurnoverBonus
                }
            }, billingEngine, bonusEngine);
        }


        public void Bonus(decimal amount, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            this.Bonus(amount, amount, billingEngine, bonusEngine);
        }

        public void Bonus(decimal amount, decimal withdrawLimit, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            this.Action(new ActionInfo()
            {
                Type = "bonus",
                Billing = new BillingAction()
                {
                    Amount = amount,
                    WithdrawLimitAmount = withdrawLimit
                }
            }, billingEngine, bonusEngine);
        }

        //public BoYu66.Services.bonusEngine BonusEngine
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }

        //    set
        //    {
        //    }
        //}
    }

}
