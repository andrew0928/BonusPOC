﻿using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{

    public abstract class RuleBase
    {
        /// <summary>
        /// 是否已經通過這個條件 (Rule) ?
        /// </summary>
        public bool IsPass { get; protected set; } = false;

        /// <summary>
        /// 這個條件的完成進度 (%, 0 ~ 100)?
        /// </summary>
        public int Progress { get; protected set; } = 0;

        public CompositeRulesEventBase CompositeRulesEventBase
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public readonly string RuleId = Guid.NewGuid().ToString();

        public abstract bool ApplyAction(ActionInfo action , EventBase e);    //todo event




        internal static bool ApplyGroupRules(EventBase e, ActionInfo action, IEnumerable<RuleBase> rules)
        {
            bool result = true;

            BonusEngine.Log($"[{e}] 資格確認進度: {action.Type}");
            foreach (RuleBase rule in rules)
            {
                if (rule.IsPass)
                {
                    BonusEngine.Log($"- [{rule}]: PASSED");
                    continue;
                }
                rule.ApplyAction(action, e);
                BonusEngine.Log($"- [{rule}]: {rule.Progress}%");
                if (rule.IsPass == false) result = false;
            }

            //BonusEngine.Log();
            return result;
        }
    }


    public class TimeRangeRule : RuleBase
    {
        public readonly DateTime SinceUTC = DateTime.MinValue;
        public readonly DateTime UntilUTC = DateTime.MaxValue;

        public override bool ApplyAction(ActionInfo action , EventBase e)
        {
            if (action.EventTimeUTC < this.SinceUTC) return false;
            if (action.EventTimeUTC > this.UntilUTC) return false;

            this.IsPass = true;
            this.Progress = 100;
            return true;
        }

        public override string ToString()
        {
            return $"時段({this.SinceUTC} ~ {this.UntilUTC})";
        }
    }

    public class FridayLoginRule : RuleBase
    {
        public override bool ApplyAction(ActionInfo action , EventBase e)
        {
            if (action.Type != "user_login") return false;
            if (action.EventTimeUTC.ToLocalTime().DayOfWeek == DayOfWeek.Friday)
            {
                this.IsPass = true;
                this.Progress = 100;
            }
            return this.IsPass;
        }

        public override string ToString()
        {
            return "週五登入";
        }
    }


    public class DepositRule : RuleBase
    {
        public decimal MinDepositAmount = 20.00M;

        public override bool ApplyAction(ActionInfo action, EventBase e)
        {
            if (action.Type != "first-deposit" && action.Type != "deposit") return false;
            if (action.Billing.Amount < this.MinDepositAmount) return false;

            this.IsPass = true;
            this.Progress = 100;
            return true;
        }


        public override string ToString()
        {
            return $"存款(至少{this.MinDepositAmount}元)";
        }
    }


    public class UserVerifyRule : RuleBase
    {
        public string VerifyType;

        public override bool ApplyAction(ActionInfo action, EventBase e)
        {
            if (action.Type != "user_verify") return false;
            if (action.Member.VerifyItem != this.VerifyType) return false;

            this.IsPass = true;
            this.Progress = 100;
            return true;
        }

        public override string ToString()
        {
            return $"身分驗證({this.VerifyType})";
        }
    }

    /// <summary>
    /// 曾經下注過某廠商
    /// </summary>
    public class PlatformCodeRule : RuleBase
    {
        public string PlatformCode;

        public override bool ApplyAction(ActionInfo action, EventBase e)
        {
            if (action.Type != "settle") return false;
            if (action.Turnover.PlatformCode != this.PlatformCode) return false;

            this.IsPass = true;
            this.Progress = 100;
            return true;
        }

        public override string ToString()
        {
            return $"特定遊戲廠商({this.PlatformCode})";
        }
    }

    public class WinningSteakTimesRule : RuleBase
    {
        public int MinTurnover = 2;   //最小啟動流水
        public int RequireWinningSteakTimes = 3;   //所需連贏次數
        public string RequirePlatformCode = "GB";   //所需投注代碼
        public bool Continuous = true;

        public int WinningSteakTimes = 0;   //目前連贏次數


        public override bool ApplyAction(ActionInfo action, EventBase e)
        {
            if (action.Type != "settle") return false;
            if (action.Turnover.Amount < this.MinTurnover) return false;
            if (action.Billing.Amount > 0 && action.Turnover.PlatformCode == RequirePlatformCode)
            {
                this.WinningSteakTimes++;
            }
            else if (Continuous)
            {
                //沒有連續就歸零
                this.WinningSteakTimes = 0;
                this.Progress = 0;
            }

            if (this.WinningSteakTimes >= this.RequireWinningSteakTimes)
            {
                this.IsPass = true;
                this.Progress = 100;
                return true;
            }
            else
            {
                this.Progress = WinningSteakTimes * 100 / RequireWinningSteakTimes;
                return false;
            }
        }

        public override string ToString()
        {
            return $"{this.RequirePlatformCode} {this.RequireWinningSteakTimes}連贏";
        }
    }

    public class LossRule : RuleBase
    {
        public int MinTurnover = 2;   //最小啟動流水
        public decimal ReqireLossAmount = 300000;   
        public string RequirePlatformCode = "";   //所需投注廠商代碼

        public decimal LossAmount = 0;   //目前負盈利金額

        public override bool ApplyAction(ActionInfo action, EventBase e)
        {
            if (action.Type != "settle") return false;
            if (action.Turnover.Amount < this.MinTurnover) return false;
            if (RequirePlatformCode != "" && action.Turnover.PlatformCode != RequirePlatformCode) return false;
            if (action.Billing.Amount < 0)
            {
                this.LossAmount += Math.Abs(action.Billing.Amount);
            }
            this.Progress = (int)(LossAmount * 100 / ReqireLossAmount) > 100 ? 100 : (int)(LossAmount * 100 / ReqireLossAmount);
            this.IsPass = this.Progress < 100 ? false : true;
            return (this.LossAmount >= ReqireLossAmount);

        }

        public override string ToString()
        {
            return $"{this.RequirePlatformCode} 負盈利 {this.ReqireLossAmount}";
        }
    }

    public class LuckyTransactionRule : RuleBase
    {
        public string Luckystr = "66";
        public string RequirePlatformCode = "";   //所需投注廠商代碼

        public string PlatformCode = "";   //投注廠商代碼

        public override bool ApplyAction(ActionInfo action, EventBase e)
        {
            if (action.Turnover == null) return false;
            if (RequirePlatformCode != "" && action.Turnover.PlatformCode != RequirePlatformCode) return false;
            if (action.Billing.TransactionId.EndsWith(this.Luckystr))   
            {
                this.IsPass = true;
                this.Progress = 100;
                return true;
            }
            this.Progress = 0;
            return true;
        }

        public override string ToString()
        {
            return $"{this.PlatformCode} 尾碼{this.Luckystr}幸運注單 ";
        }
    }

    //public class CountRule : RuleBase
    //{
    //    public int LimitCounts = 1;
    //    public string BonusName = "身分驗證紅利";
    //    public override bool ApplyAction(ActionInfo action , EventBase e)
    //    {
    //        //action.Bonus
    //        //completeBonus


    //        if (e.BonusEngine.completeBonus[BonusName] >= LimitCounts)
    //        {
    //            this.IsPass = false;
    //            this.Progress = 0;
    //            return false;
    //        }
    //        else
    //        {
    //            this.IsPass = true;
    //            this.Progress = 100;
    //            return true;
    //        }
    //        //return true;

    //    }
    //}

    //-----------------------------------------------------

    //public class RequireTurnoverRule : RuleBase
    //{
    //    public string RequireTurnoverPlatformCode = null;

    //    //public decimal RequireTurnoverAmount = 0.00M;

    //    public decimal LeftTurnoverAmount = 0.00M;

    //    //public string RequirePlatformCode = "";   //所需投注廠商代碼

    //    public string PlatformCode = "";   //投注廠商代碼


    //    public override bool ApplyAction(ActionInfo action)
    //    {
    //        if (RequireTurnoverPlatformCode != "" && action.Turnover.PlatformCode != RequireTurnoverPlatformCode) return false;

    //        if (action.Turnover != null && action.Turnover.Amount > 0 && (this.RequireTurnoverPlatformCode == null || this.RequireTurnoverPlatformCode == action.Turnover.PlatformCode))
    //        {
    //            decimal diff = Math.Min(action.Turnover.Amount, this.LeftTurnoverAmount);
    //            action.Turnover.Amount -= diff;
    //            this.LeftTurnoverAmount -= diff;
    //        }


    //            //decimal diff = BillingEngine.Current.CurrentAccountInfo.TurnoverAmount;
    //            //BillingEngine.Current.Bonus((decimal)((double)diff * BonusAmountTurnoverRate));
    //            //BillingEngine.Current.CurrentAccountInfo.TurnoverAmount -= diff;


    //        this.IsPass = (this.LeftTurnoverAmount == 0);
    //        this.Progress = this.IsPass ? 100 : 0;  //(int)(this.LeftTurnoverAmount * 100 / this.RequireTurnoverAmount);
    //        return this.IsPass;
    //    }
    //}
}
