﻿using BoYu66.Services.Billing;
using BoYu66.Services.Bonus;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Membership
{
    public class MemberEngine : EngineBase<MemberEngine>
    {

        public event ActionEventHandler RegUserEvent = (action, billingEngine, bonusEngine) => { };
        public event ActionEventHandler VerifyEvent = (action, billingEngine, bonusEngine) => { };
        public event ActionEventHandler UserLoginEvent = (action, billingEngine, bonusEngine) => { };


        protected override void Action(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            bonusEngine.AddLogList(action.Type + " - " + action.Member.VerifyItem);

            switch (action.Type)
            {
                case "user_reg":
                    this.RegUserEvent(action , billingEngine , bonusEngine);
                    break;

                case "user_verify":
                    this.VerifyEvent(action, billingEngine, bonusEngine);
                    break;

                case "user_login":
                    this.UserLoginEvent(action, billingEngine, bonusEngine);
                    break;
            }

            Console.WriteLine(action.GetDataLine() + " | " + billingEngine.CurrentAccountInfo.GetDataLine());
        }




        public void RegisterAccount(BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            //
            // do create user account
            //

            //BonusEngine.Current.RegisterEvent(new IdVerifyEvent());
            //BonusEngine.Current.RegisterEvent(new PhoneVerifyEvent());
            //BonusEngine.Current.RegisterEvent(new EmailVerifyEvent());

            //BonusEngine.Current.RegisterEvent(new DepositEvent("first-deposit", 0, 2, 10, 30, 2000, 100));

            //BonusEngine.Current.RegisterEvent(new DepositEvent(new BonusRuleModel { EventTypeID = "first-deposit", BonusAmount = 0, WithdrawLimitRate = 2, TurnoverLimitRate = 10, BonusRate = 30, MaxBonusAmount = 2000, MinDepositAmount = 100 }));

            //BonusEngine.Current.RegisterEvent(new CompositeRulesEventBase("EVENT-IDVERIFY", 1, 18, 1, 0)
            //{
            //    _name = "身分驗證紅利",
            //    MatchRules = new List<RuleBase>()
            //    {
            //        new UserVerifyRule() { VerifyType = "id-verify" },
            //        new DepositRule() { MinDepositAmount = 100 }
            //    }
            //});

            //BonusEngine.Current.RegisterEvent(new CompositeRulesEventBase("EVENT-MAILVERIFY", 1, 10, 1, 0)
            //{
            //    _name = "郵件驗證紅利",
            //    MatchRules = new List<RuleBase>()
            //    {
            //        new UserVerifyRule() { VerifyType = "email-verify" },
            //        new DepositRule() { MinDepositAmount = 100 }
            //    }
            //});

            //BonusEngine.Current.RegisterEvent(new CompositeRulesEventBase("EVENT-PHONEVERIFY", 1, 10, 1, 0)
            //{
            //    _name = "電話驗證紅利",
            //    MatchRules = new List<RuleBase>()
            //    {
            //        new UserVerifyRule() { VerifyType = "phone-verify" },
            //        new DepositRule() { MinDepositAmount = 100 }
            //    }
            //});

            this.Action(new ActionInfo()
            {
                Type = "user_reg",
                Member = new MemberAction()
                {
                    
                }
            } , billingEngine, bonusEngine);
        }

        public void RegisterAccount(BonusRuleModel firstDepositBonus , BonusRuleModel idVerifyBonus, BonusRuleModel emailVerifyBonus , BonusRuleModel phoneVerifyBonus , BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            //
            // do create user account
            //

            //BonusEngine.Current.RegisterEvent(new IdVerifyEvent());
            //BonusEngine.Current.RegisterEvent(new PhoneVerifyEvent());
            //BonusEngine.Current.RegisterEvent(new EmailVerifyEvent());

            bonusEngine.RegisterEvent(new DepositEvent(firstDepositBonus));

            bonusEngine.RegisterEvent(new CompositeRulesEventBase("EVENT-IDVERIFY", 1, idVerifyBonus.BonusAmount, idVerifyBonus.WithdrawLimitRate, idVerifyBonus.TurnoverLimitRate)
            {
                _name = "身分驗證紅利",
                MatchRules = new List<RuleBase>()
                {
                    new UserVerifyRule() { VerifyType = "id-verify" },
                    new DepositRule() { MinDepositAmount = 100 }
                }
            });

            bonusEngine.RegisterEvent(new CompositeRulesEventBase("EVENT-EMAILVERIFY", 1, emailVerifyBonus.BonusAmount, emailVerifyBonus.WithdrawLimitRate, emailVerifyBonus.TurnoverLimitRate)
            {
                _name = "郵件驗證紅利",
                MatchRules = new List<RuleBase>()
                {
                    new UserVerifyRule() { VerifyType = "email-verify" },
                    new DepositRule() { MinDepositAmount = 100 }
                }
            });

            bonusEngine.RegisterEvent(new CompositeRulesEventBase("EVENT-PHONEVERIFY", 1, phoneVerifyBonus.BonusAmount, phoneVerifyBonus.WithdrawLimitRate, phoneVerifyBonus.TurnoverLimitRate)
            {
                _name = "電話驗證紅利",
                MatchRules = new List<RuleBase>()
                {
                    new UserVerifyRule() { VerifyType = "phone-verify" },
                    new DepositRule() { MinDepositAmount = 100 }
                }
            });

            this.Action(new ActionInfo()
            {
                Type = "user_reg",
                Member = new MemberAction()
                {

                }
            } , billingEngine , bonusEngine);
        }


        public void VerifyAccount(string type , BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            this.Action(new ActionInfo()
            {
                Type = "user_verify",
                Member = new MemberAction()
                {
                    VerifyItem = type
                }
            } , billingEngine , bonusEngine);
        }

        public void Login(BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            this.Action(new ActionInfo()
            {
                Type = "user_login",
                Member = new MemberAction()
                { }
            }, billingEngine, bonusEngine);
        }

        public void Login()
        {
            //this.Action(new ActionInfo()
            //{
            //    Type = "user_login",
            //    Member = new MemberAction()
            //    { }
            //});
        }
    }
}
