﻿using BoYu66.Services.Billing;
using BoYu66.Services.Bonus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Common
{
    public delegate void ActionEventHandler(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine);



    public abstract class EngineBase<T>
    {
        private static T _current_engine;

        public static void Init(T instance)
        {
            _current_engine = instance;
        }

        public static T Current
        {
            get
            {
                return _current_engine;
            }
        }

        //protected abstract void Action(ActionInfo action);
        protected abstract void Action(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine);
    }
}
