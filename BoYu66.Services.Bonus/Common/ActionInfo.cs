﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Common
{

    public class ActionInfo
    {
        /// <summary>
        /// billing:    deposit, withdraw, transfer_in, transfer_out, bet, settle
        /// membership: user_reg, user_verify
        /// </summary>
        public string Type;

        //public string PlatformCode;
        public DateTime EventTimeUTC = DateTime.UtcNow;


        public BillingAction Billing = null;

        /// <summary>
        /// generated turnover
        /// </summary>
        public TurnoverAction Turnover = null;

        public MemberAction Member = null;






        public string GetHeaderLine()
        {
            return string.Format("動作                 金額      ");
        }

        public string GetSplitLine()
        {
            return string.Format("-------------------- ----------");
        }

        public string GetDataLine()
        {
            string item = this.Type;

            if (this.Turnover != null && string.IsNullOrEmpty(this.Turnover.PlatformCode) == false) item += $"({this.Turnover.PlatformCode})";

            return string.Format(
                "{0,20} {1,10}",
                //this.Type,
                item,
                (this.Billing == null)?("--"):this.Billing.Amount.ToString());
        }
    }

    public class MemberAction
    {
        public string MainAccountId;
        public string MainAccountSn;
        public string VerifyItem;
    }


    public class BillingAction
    {


        /// <summary>
        /// action amount
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// generated withdraw limit
        /// </summary>
        public decimal WithdrawLimitAmount;

        /// <summary>
        /// 對應的交易單號
        /// </summary>
        public string TransactionId = string.Format("TID-{0:N}", Guid.NewGuid());
    }



    public class TurnoverAction
    {
        /// <summary>
        /// 下注金額 (用來沖銷提款限額)
        /// </summary>
        //public decimal BetAmount;

        /// <summary>
        /// 可被沖銷的流水金額
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// 館別代碼
        /// </summary>
        public string PlatformCode;

        /// <summary>
        /// 是否能套用到有效流水?
        /// </summary>
        public bool ForEffectTurnoverAmount;


        /// <summary>
        /// 是否能套用到返水紅利?
        /// </summary>
        public bool ForTurnoverBonus;

        /// <summary>
        /// 是否能套用到其他特殊紅利?
        /// </summary>
        public bool ForBonusEvent;

    }

}
