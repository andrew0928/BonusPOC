﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    public class BonusRuleModel //:EventBase
    {
        /// <summary>
        /// 紅利編號
        /// </summary>
        public string EventTypeID { get; set; }
        /// <summary>
        /// 紅利金額
        /// </summary>
        public decimal BonusAmount { get; set; }
        /// <summary>
        /// 可重複次數
        /// </summary>
        public int RepeatCountLimit { get; set; }
        /// <summary>
        /// 提款限額
        /// </summary>
        public int WithdrawLimitRate { get; set; }
        /// <summary>
        /// 流水限制倍
        /// 領取紅利後須打滿多少紅利倍數的流水才能領取紅利
        /// </summary>
        public int TurnoverLimitRate { get; set; }
        /// <summary>
        /// 紅利倍數
        /// (動態紅利金額)依照觸發金額的％數發放紅利金額
        /// </summary>
        public decimal BonusRate { get; set; }
        /// <summary>
        /// 該項紅利最高可領取金額
        /// </summary>
        public decimal MaxBonusAmount { get; set; }
        /// <summary>
        /// 最低存款金額
        /// </summary>
        public decimal MinDepositAmount { get; set; }
        /// <summary>
        /// 紅利進行的順序
        /// 數字越小代表較優先
        /// </summary>
        public int Priority { get; set; }
        /// <summary>
        /// 指定特定廠商
        /// </summary>
        public string RequirePlatformCode { get; set; }

        /// <summary>
        /// 最小啟動流水
        /// 必須大於最小啟動流水才符合領取紅利資格
        /// </summary>
        public int MinTurnover { get; set; }
        /// <summary>
        /// 累積負營利金額
        /// </summary>
        public decimal ReqireLossAmount { get; set; }
        /// <summary>
        /// 指定字串
        /// </summary>
        public string Luckystr { get; set; }
        /// <summary>
        /// 流水紅利%數
        /// </summary>
        public double BonusAmountTurnoverRate { get; set; }
        /// <summary>
        /// 連贏次數
        /// </summary>
        public int RequireWinningSteakTimes { get; set; }


    }
}
