﻿using BoYu66.Services.Billing;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{

    [Obsolete]
    public class _DepositEvent : CompositeRulesEventBase
    {
        public _DepositEvent()
        {
            this.BonusAmount = 20;
            this.WithdrawLimitRate = 1;
            this.TurnoverLimitRate = 10;

            this.MatchRules.Add(new DepositRule() { MinDepositAmount = 100 });
            this.MatchRules.Add(new FridayLoginRule() { });
        }

        public override string ToString()
        {
            return "用戶存款紅利";
        }
    }




    /// <summary>
    /// 首存紅利 event, 至少存 20
    /// </summary>
    [Obsolete]
    public class _FirstDepositEvent : SettleEventBase
    {
        public readonly decimal MinDepositAmount = 20;

        public _FirstDepositEvent()
        {
            this.MinDepositAmount = 100;
            this.BonusRate = 20;    //New
            this.MaxBonusAmount = 2000; //New
            //this.BonusAmount = 15;
            this.WithdrawLimitRate = 1;


            this.TurnoverLimitRate = 10;
        }

        public override bool IsMatchAndInit(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            if (action.Type == "first-deposit" && action.Billing.Amount >= this.MinDepositAmount)
            {
                //this.BonusAmount = this.FirstDepositBonusAmount; // 首存紅利固定 15 元
                decimal tmpBonusAmount = (action.Billing.Amount * (this.BonusRate / 100));
                this.BonusAmount = tmpBonusAmount >= this.MaxBonusAmount ? this.MaxBonusAmount : tmpBonusAmount;
                return base.IsMatchAndInit(action , billingEngine , bonusEngine);
            }
            return false;
        }

        public override string ToString()
        {
            return $"首存紅利，存滿{this.MinDepositAmount}以上送{this.BonusRate}%紅利金，最高上限{this.MaxBonusAmount}紅利金";
        }
    }


}
