﻿using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    public class WinningSteakTimesEvent : CompositeRulesEventBase
    {
        public readonly int MinTurnover = 2;   //最小啟動流水
        public readonly int RequireWinningSteakTimes = 3;   //所需連贏次數
        public readonly string RequirePlatformCode = "GB";   //所需投注代碼
        public readonly bool Continuous = true;

        public int WinningSteakTimes = 0;   //目前連贏次數
        public string platformCode = "";   //投注代碼

        public WinningSteakTimesEvent(string platformCode)
        {

            //this.Priority = 89998;
            //this.BonusAmount = 300;
            //this.WithdrawLimitRate = 3;

            this.MatchRules.Add(new WinningSteakTimesRule() { MinTurnover = this.MinTurnover, RequireWinningSteakTimes = this.RequireWinningSteakTimes , RequirePlatformCode = this.RequirePlatformCode, Continuous = this.Continuous });
            //次數限制 區間限制
            //this.MatchRules.Add(new CountRule() { LimitCounts = 3 });
        }

        public WinningSteakTimesEvent(BonusRuleModel bonusSetting)
        {
            this.Priority = bonusSetting.Priority;
            this.BonusAmount = bonusSetting.BonusAmount;
            this.WithdrawLimitRate = bonusSetting.WithdrawLimitRate;
            this.MinTurnover = bonusSetting.MinTurnover;
            this.RequireWinningSteakTimes = bonusSetting.RequireWinningSteakTimes;
            this.RequirePlatformCode = bonusSetting.RequirePlatformCode;
            this.TurnoverLimitRate = bonusSetting.TurnoverLimitRate;
            this.RepeatCountLimit = bonusSetting.RepeatCountLimit;

            this.MatchRules.Add(new WinningSteakTimesRule() { MinTurnover = this.MinTurnover, RequireWinningSteakTimes = this.RequireWinningSteakTimes, RequirePlatformCode = this.RequirePlatformCode, Continuous = this.Continuous });

        }


        public override string ToString()
        {
            return $"{this.platformCode}連贏";

        }
    }

}
