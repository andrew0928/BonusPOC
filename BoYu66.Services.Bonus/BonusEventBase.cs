﻿using BoYu66.Services.Billing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    /// <summary>
    /// 完成後可以領到紅利的活動
    /// </summary>
    public abstract class BonusEventBase : EventBase
    {
        /// <summary>
        /// 完成活動可領取的紅利金額
        /// </summary>
        public decimal BonusAmount { get; protected set; }

        public override void Complete(BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            billingEngine.CurrentAccountInfo.ActualBalance += this.BonusAmount;
            billingEngine.CurrentAccountInfo.WithdrawLimit += this.BonusAmount;

            //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 領取紅利: {this.BonusAmount}");   //所增加的提款限額
            bonusEngine.AddLogList($"{this} : 領取紅利: {this.BonusAmount}");

        }
    }
}
