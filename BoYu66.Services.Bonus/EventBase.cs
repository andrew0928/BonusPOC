﻿using BoYu66.Services.Billing;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    /// <summary>
    /// 活動 Base 類別
    /// 
    /// 申請活動: 將該 rule 加入 rules 清單內，並且正確的排序
    /// 更新進度: Apply
    /// 領取獎勵: Complete
    /// </summary>
    public abstract class EventBase
    {

        /// <summary>
        /// 排序
        /// </summary>
        public int Priority = 100;

        /// <summary>
        /// 識別紅利活動 TYPE 用的 ID。每 "一種" 紅利活動，會有一個唯一的 ID。
        /// </summary>
        public string EventTypeID = "eventtype-" + Guid.NewGuid().ToString("N");

        /// <summary>
        /// 是否已可領取?
        /// </summary>
        public bool IsReadyForComplete { get; protected set; }

        /// <summary>
        /// 可以重複註冊活動的次數限制。
        /// 0 代表不限制
        /// </summary>
        public int RepeatCountLimit = 1;

        /// <summary>
        /// 是否符合紅利沖銷流水的條件? 如果符合，會自動註冊 (registered), 必須同時做 rules 的 init 動作
        /// </summary>
        public abstract bool IsMatchAndInit(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine);

        /// <summary>
        /// apply turnover to event
        /// </summary>
        /// <param name="amountApply">max turnover can be applied for this event</param>
        /// <returns>actual turnover amount taken</returns>
        public abstract void ApplyAction(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine);

        /// <summary>
        /// 領取紅利
        /// </summary>
        /// <param name="account"></param>
        public virtual void Complete(BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            bonusEngine.IncrementEventCompleteCount(this.EventTypeID);
        }
    }





    public class CompositeRulesEventBase : EventBase
    {
        public string _name = null;





        /// <summary>
        /// 符合資格
        /// </summary>
        public List<RuleBase> MatchRules = new List<RuleBase>();
        //public List<RuleBase> PassRules = new List<RuleBase>();

        /// <summary>
        /// 領取前必須沖銷的流水金額。若此數值 > 0 則代表這是 "先發紅利"。
        /// </summary>
        public decimal RequiredTurnoverAmount { get; protected set; }

        /// <summary>
        /// 領取紅利的所需流水 (還剩多少)
        /// </summary>
        public decimal TurnoverAmountLeft { get; private set; }

        /// <summary>
        /// 完成活動可領取的紅利金額
        /// </summary>
        public decimal BonusAmount { get; protected set; }

        /// <summary>
        /// 紅利金額比例(百分之多少)
        /// </summary>
        public decimal BonusRate { get; protected set; }

        /// <summary>
        /// 最大紅利金額
        /// </summary>
        public decimal MaxBonusAmount { get; protected set; }

        /// <summary>
        /// 流水限制倍
        /// </summary>
        public int TurnoverLimitRate { get; protected set; }

        /// <summary>
        /// 提款限制倍
        /// </summary>
        public int WithdrawLimitRate { get; protected set; }

        public CompositeRulesEventBase()
        {

        }

        public CompositeRulesEventBase(string eventID, int repeat_limit, decimal bonus, int withdraw_rate, int turnover_rate)
        {
            this.EventTypeID = eventID;
            this.RepeatCountLimit = repeat_limit;

            this.BonusAmount = bonus;
            this.WithdrawLimitRate = withdraw_rate;
            this.TurnoverLimitRate = turnover_rate;
        }

        public CompositeRulesEventBase(BonusRuleModel bonusSeeting)
        {
            this.EventTypeID = bonusSeeting.EventTypeID;
            this.RepeatCountLimit = bonusSeeting.RepeatCountLimit;

            this.BonusAmount = bonusSeeting.BonusAmount;
            this.WithdrawLimitRate = bonusSeeting.WithdrawLimitRate;
            this.TurnoverLimitRate = bonusSeeting.TurnoverLimitRate;
        }


        public override bool IsMatchAndInit(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            bool match = RuleBase.ApplyGroupRules(this, action, this.MatchRules);

            if (match)
            {
                this.TurnoverAmountLeft = this.RequiredTurnoverAmount = this.BonusAmount * this.TurnoverLimitRate;
                billingEngine.CurrentAccountInfo.BonusWallet += this.BonusAmount;
                billingEngine.CurrentAccountInfo.TurnoverAmount -= this.BonusAmount * this.TurnoverLimitRate;
                //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# 申請活動 [{this.ToString()}]: 打滿流水 {this.RequiredTurnoverAmount}, 可領取紅利 {this.BonusAmount}");

                bonusEngine.AddLogList($"{this} 已滿足資格，可開始沖銷流水。需打滿流水 {this.RequiredTurnoverAmount}, 即可領取紅利 {this.BonusAmount}, 同時提高提款限額 {this.WithdrawLimitRate * this.BonusAmount}");
            }


            return match;
        }

        public override void ApplyAction(ActionInfo action , BillingEngine billingEngine , BonusEngine bonusEngine)
        {
            //this.IsReadyForComplete = RuleBase.ApplyGroupRules(action, this.PassRules);
            if (action.Turnover == null || action.Turnover.Amount == 0)
            {

            }
            else
            {
                decimal diff = Math.Min(action.Turnover.Amount, this.TurnoverAmountLeft);

                //
                // 沖銷流水
                //
                bonusEngine.AddLogList($"沖銷流水至 [{this}]: {action.Type} 有效流水 {action.Turnover.Amount} (from: {action.Turnover.PlatformCode}");
                //BonusEngine.Log($"沖銷流水至 [{this}]: {action.Type} 有效流水 {action.Turnover.Amount} (from: {action.Turnover.PlatformCode})");
                action.Turnover.Amount -= diff;
                this.TurnoverAmountLeft -= diff;
                billingEngine.CurrentAccountInfo.TurnoverAmount += diff;
                //BillingEngine.Current.CurrentAccountInfo.TurnoverAmount += diff;


                var logStr = (this.TurnoverAmountLeft != 0 && this.RequiredTurnoverAmount != 0) ? $",進度 { this.TurnoverAmountLeft} / { this.RequiredTurnoverAmount}" : "";
                bonusEngine.AddLogList($"- 已沖銷流水 {diff}, 剩餘可用流水 {action.Turnover.Amount} {logStr}");
                //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 沖銷流水: {diff} ，　剩餘流水:{this.TurnoverAmountLeft}");
            }

            this.IsReadyForComplete = (this.TurnoverAmountLeft == 0);


            if (this.IsReadyForComplete)
            {
                //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 已完成活動條件");
                bonusEngine.AddLogList($" - 已完成活動所需流水條件，可以領取紅利。");
            }
            //BonusEngine.Log();
        }

        public override void Complete(BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 領取紅利: {this.BonusAmount}");   //所增加的提款限額
            billingEngine.CurrentAccountInfo.ActualBalance += this.BonusAmount;
            billingEngine.CurrentAccountInfo.WithdrawLimit += this.BonusAmount * this.WithdrawLimitRate;
            billingEngine.CurrentAccountInfo.BonusWallet = Math.Max(0, billingEngine.CurrentAccountInfo.BonusWallet - this.BonusAmount);

            bonusEngine.AddLogList($" 領取活動紅利: {this}, {this.BonusAmount}");
            base.Complete(billingEngine , bonusEngine);
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(this._name) == false) return this._name;
            return base.ToString();
        }
    }

}
