﻿using BoYu66.Services.Billing;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    /// <summary>
    /// 各種類型的反水紅利。所有需要用流水打限額才能取得的紅利都算此類
    /// 支援先發 / 後發紅利
    /// </summary>
    public abstract class SettleEventBase : EventBase
    {
        /// <summary>
        /// 領取前必須沖銷的流水金額。若此數值 > 0 則代表這是 "先發紅利"。
        /// </summary>
        public decimal RequiredTurnoverAmount { get; protected set; }

        /// <summary>
        /// 領取紅利的所需流水 (還剩多少)
        /// </summary>
        public decimal TurnoverAmountLeft { get; private set; }

        /// <summary>
        /// 完成活動可領取的紅利金額
        /// </summary>
        public decimal BonusAmount { get; protected set; }
        
        /// <summary>
        /// 紅利金額比例(百分之多少)
        /// </summary>
        public decimal BonusRate { get; protected set; }

        /// <summary>
        /// 最大紅利金額
        /// </summary>
        public decimal MaxBonusAmount { get; protected set; }

        /// <summary>
        /// 流水限制倍
        /// </summary>
        public int TurnoverLimitRate { get; protected set; }

        /// <summary>
        /// 提款限制倍
        /// </summary>
        public int WithdrawLimitRate { get; protected set; }

        public override bool IsMatchAndInit(ActionInfo action , BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            this.TurnoverAmountLeft = this.RequiredTurnoverAmount = this.BonusAmount * this.TurnoverLimitRate;

            billingEngine.CurrentAccountInfo.BonusWallet += this.BonusAmount;
            billingEngine.CurrentAccountInfo.TurnoverAmount -= this.BonusAmount * this.TurnoverLimitRate;
            //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# 申請活動 [{this.ToString()}]: 打滿流水 {this.RequiredTurnoverAmount}, 可領取紅利 {this.BonusAmount}");
            BonusEngine.Log($"# 申請活動 [{this.ToString()}]: 打滿流水 {this.RequiredTurnoverAmount}, 可領取紅利 {this.BonusAmount}");

            return true;
        }

        public override void ApplyAction(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            if (action.Turnover == null) return;
            if (action.Turnover.Amount == 0) return;

            decimal diff = Math.Min(action.Turnover.Amount, this.TurnoverAmountLeft);

            //
            // 沖銷流水
            //
            action.Turnover.Amount -= diff;
            this.TurnoverAmountLeft -= diff;
            billingEngine.CurrentAccountInfo.TurnoverAmount += diff;

            this.IsReadyForComplete = (this.TurnoverAmountLeft == 0);

            //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 沖銷流水: {diff} ，　剩餘流水:{this.TurnoverAmountLeft}");
            BonusEngine.Log($"# [{this}] 沖銷流水: {diff} ，　剩餘流水:{this.TurnoverAmountLeft}");

            if (this.IsReadyForComplete)
            {
                //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 已完成活動條件");
                BonusEngine.Log($"{string.Empty.PadLeft(78, ' ')}# [{this}] 已完成活動條件");
            }


        }

        public override void Complete(BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            billingEngine.CurrentAccountInfo.ActualBalance += this.BonusAmount;
            billingEngine.CurrentAccountInfo.WithdrawLimit += this.BonusAmount * this.WithdrawLimitRate;
            billingEngine.CurrentAccountInfo.BonusWallet = Math.Max(0, billingEngine.CurrentAccountInfo.BonusWallet - this.BonusAmount);

            //Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] 領取紅利: {this.BonusAmount}");
            BonusEngine.Log($"# [{this}] 領取紅利: {this.BonusAmount}");

        }
    }
}
