﻿using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{
    public class LossEvent : CompositeRulesEventBase
    {
        public readonly int MinTurnover = 2;   //最小啟動流水
        public readonly decimal ReqireLossAmount = 1000;   //所需負盈利金額
        public readonly string RequirePlatformCode = "GB";   //所需投注代碼

        public LossEvent()
        {

            //this.Priority = 89998;
            //this.BonusAmount = 500;
            //this.WithdrawLimitRate = 10;

            this.MatchRules.Add(new LossRule() { MinTurnover = this.MinTurnover, ReqireLossAmount = this.ReqireLossAmount});
            //次數限制 區間限制
            //this.MatchRules.Add(new CountRule() { LimitCounts = 3 });
            //this.MatchRules.Add(new PlatformCodeRule() { PlatformCode = RequirePlatformCode });

        }

        public LossEvent(BonusRuleModel bonusSetting)
        {
            this.Priority = bonusSetting.Priority;
            this.BonusAmount = bonusSetting.BonusAmount;
            this.WithdrawLimitRate = bonusSetting.WithdrawLimitRate;
            this.MinTurnover = bonusSetting.MinTurnover;
            this.ReqireLossAmount = bonusSetting.ReqireLossAmount;
            this.RequirePlatformCode = bonusSetting.RequirePlatformCode;
            this.RepeatCountLimit = bonusSetting.RepeatCountLimit;

            this.MatchRules.Add(new LossRule() { MinTurnover = this.MinTurnover, ReqireLossAmount = this.ReqireLossAmount, RequirePlatformCode = this.RequirePlatformCode });

        }


        public override string ToString()
        {
            return $"負營利 {ReqireLossAmount}";

        }
    }


    //[Obsolete]
    //public class _LossEvent : SettleEventBase
    //{
    //    public readonly int MinTurnover = 2;   //最小啟動流水
    //    public decimal LossAmount = 0;   //目前負盈利金額
    //    public readonly decimal ReqireLossAmount = 10000;   //所需負盈利金額

    //    public _LossEvent()
    //    {
    //        this.Priority = 89998;
    //        this.BonusAmount = 500;
    //        this.WithdrawLimitRate = 10;
    //    }

    //    public _LossEvent(string platformCode)
    //    {
    //        this.Priority = 89998;
    //        this.BonusAmount = 500;
    //        this.WithdrawLimitRate = 10;
    //    }


    //    public override void ApplyAction(ActionInfo action)
    //    {
    //        if (action.Billing.Amount < 0)
    //        {
    //            this.LossAmount += Math.Abs(action.Billing.Amount);
    //        }
         
    //        this.IsReadyForComplete = (this.LossAmount >= ReqireLossAmount);

    //        Console.WriteLine($"{string.Empty.PadLeft(78, ' ')}# [{this}] Progress: {this.LossAmount} / {this.ReqireLossAmount}, IsComplete: {this.IsReadyForComplete}.");
    //    }


    //    public override bool IsMatchAndInit(ActionInfo action)
    //    {
    //        if (action.Turnover == null) return false;
    //        if (action.Turnover.Amount <= 0) return false;
    //        switch (action.Type)
    //        {
    //            case "settle":
    //                return base.IsMatchAndInit(action);
    //        }
    //        return false;
    //    }

    //    public override string ToString()
    //    {
    //        return $"復活金-負盈利{this.ReqireLossAmount}以上，贈予復活金{this.BonusAmount}，提款限額{this.WithdrawLimitRate}倍";
    //    }
    //}
}
