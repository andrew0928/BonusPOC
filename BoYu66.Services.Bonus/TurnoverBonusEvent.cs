﻿using BoYu66.Services.Billing;
using BoYu66.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoYu66.Services.Bonus
{

    /// <summary>
    /// 反水紅利
    /// </summary>
    public class TurnoverBonusEvent : SettleEventBase
    {
        public readonly int MinTurnover = 10;   //最小啟動流水
        public readonly double BonusAmountTurnoverRate;// = 0.01; //流水轉換紅利倍率

        public readonly string RequiredPlatformCode = null;


        public TurnoverBonusEvent()
        {
            this.Priority = 99999;
            this.MinTurnover = 10;
            this.BonusAmountTurnoverRate = 0.01;

            this.RequiredPlatformCode = null;
        }

        public TurnoverBonusEvent(string platformCode)
        {
            this.Priority = 99999;
            this.MinTurnover = 10;
            this.BonusAmountTurnoverRate = 0.0038;
            this.WithdrawLimitRate = 1;

            this.RequiredPlatformCode = platformCode;
        }

        public TurnoverBonusEvent(BonusRuleModel bonusSeeting)
        {
            this.Priority = 99999;
            this.MinTurnover = bonusSeeting.MinTurnover;
            this.BonusAmountTurnoverRate = bonusSeeting.BonusAmountTurnoverRate;
            this.WithdrawLimitRate = bonusSeeting.WithdrawLimitRate;
            this.RequiredPlatformCode = bonusSeeting.RequirePlatformCode;
        }


        public override void ApplyAction(ActionInfo action, BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            if (action.Turnover == null) return;
            if (this.RequiredPlatformCode != null && action.Turnover != null && this.RequiredPlatformCode != action.Turnover.PlatformCode) return;


            if (action.Turnover.Amount > MinTurnover)
            {
                decimal diff = action.Turnover.Amount;
                billingEngine.Bonus((decimal)((double)diff * BonusAmountTurnoverRate), billingEngine , bonusEngine);
                //Console.WriteLine("                                                                              # APPLY {1}: {0}", ((decimal)((double)diff * BonusAmountTurnoverRate)), this);
                bonusEngine.AddLogList($"{this} : {((decimal)((double)diff * BonusAmountTurnoverRate))}");

                action.Turnover.Amount -= diff;
            }

            // 永遠不能 complete
            this.IsReadyForComplete = false;
        }

        public override void Complete(BillingEngine billingEngine, BonusEngine bonusEngine)
        {
        }

        public override bool IsMatchAndInit(ActionInfo action , BillingEngine billingEngine, BonusEngine bonusEngine)
        {
            if (action.Turnover == null) return false;
            if (action.Turnover.Amount <= 0) return false;
            switch (action.Type)
            {
                case "settle":
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"反水紅利({this.RequiredPlatformCode})";
        }
    }
}
